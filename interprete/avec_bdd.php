<?php
try
{
$ip = gethostbyaddr($_SERVER['REMOTE_ADDR']);
include("connexion/info_connexion".$ip.".php"); // Connexion à la base de données + style
}

catch(Exception $e)
{
        die('Erreur : '.$e->getMessage());
}

// Conditions pour récupérer l'ID du connecteur

		if((substr($_POST['formule'],5,2) == 'AC' XOR substr($_POST['formule'],6,2) == 'AC') OR (substr($_POST['formule'],5,2) == 'AL' XOR substr($_POST['formule'],6,2) == 'AL') OR (substr($_POST['formule'],5,2) == 'SR' XOR substr($_POST['formule'],6,2) == 'SR')) // AJOUTER POUR Office 2019 =@RIK en cours + dans "Interprete.php"
		{
			$init_formule = '("';
			$fin_nom_connecteur = '__';
			$nb_carac_connecteur_debut = strpos($_POST['formule'],$init_formule);
			
			if($nb_carac_connecteur_debut <> NULL) // Si on a une valeur
			{
				$nb_carac_connecteur_fin = strpos($_POST['formule'],$fin_nom_connecteur);
				$nom_connecteur = substr($_POST['formule'],($nb_carac_connecteur_debut+2), ($nb_carac_connecteur_fin-$nb_carac_connecteur_debut-2));
				
				// Requête pour récupérer le nom du connecteur
				$nom_reel_connecteur = $bdd->query("
				SELECT TOP 1
				DESIGNATION as desig
				FROM
				DL_CONNECTEURS
				WHERE
				ID_DL = '".$nom_connecteur."'
				");
				$nom_connecteur_reel = $nom_reel_connecteur->fetch();
				$vrai_nom_connecteur = $nom_connecteur_reel['desig'];
				$nom_du_connecteur = 'Connecteur : "<strong>'.$vrai_nom_connecteur.'</strong>" <em>(ID : "<strong>'.$nom_connecteur.'</strong>")</em> <br/><br/>';
		
			}	
			
			
			else
			{
				$nom_du_connecteur = 'ID du Connecteur introuvable !<br><br>';
			}
		}
		
		else
		{
			$nom_du_connecteur = '';
		}
		
		
		// Conditions pour récupérer l'ID du modèle
		
		if((substr($_POST['formule'],5,2) == 'AC' XOR substr($_POST['formule'],6,2) == 'AC') OR (substr($_POST['formule'],5,2) == 'AL' XOR substr($_POST['formule'],6,2) == 'AL') OR (substr($_POST['formule'],5,2) == 'SR' XOR substr($_POST['formule'],6,2) == 'SR'))
		{
			$init_modele = ';';
			$fin_nom_modele = '@';
			$nb_carac_modele_debut = strpos($_POST['formule'],$init_modele); 
			
			if($nb_carac_modele_debut <> NULL) // Si on a une valeur
			{
			$nb_carac_modele_fin = strpos(substr($_POST['formule'],2,9999999999),$fin_nom_modele);
			$nom_modele = substr(substr($_POST['formule'],2,9999999999),($nb_carac_modele_debut-1), ($nb_carac_modele_fin-$nb_carac_modele_debut+1)); // A CORRIGER
			
			// Requête pour récupérer le nom du modèle
				$nom_reel_modele = $bdd->query("
				SELECT TOP 1
				DESIGNATION as desig
				FROM
				DL_VUES
				WHERE
				ID_VUE = '".$nom_modele."' AND ID_DL = '".$nom_connecteur."'
				");
				$nom_modele_reel = $nom_reel_modele->fetch();
				$vrai_nom_modele = $nom_modele_reel['desig'];
				$nom_du_modele = 'Modèle : "<strong>'.$vrai_nom_modele.'</strong>" <em>(ID : "<strong>'.$nom_modele.'</strong>")</em> <br/><br/>';

			}			
			
			else
			{
				$nom_du_modele = 'ID du Modèle Introuvable !<br><br>';
			}	
		}
		
		else
		{
			$nom_du_modele = '';
		}


		// Conditions pour les détails du champ de la formule : ACE sur cette page

		if((substr($_POST['formule'],5,2) == 'AC' XOR substr($_POST['formule'],6,2) == 'AC'))
		{
			$formule_sans_reliures = str_replace('"&"','',$_POST['formule']); // On enlève les reliures
			$formule_sans_reliures = str_replace('=@RI','=RI',$_POST['formule']); // Pour gérer les nouvelles formules =@
			$nb_carac_montant_debut = strpos($formule_sans_reliures,'L='); // Check de présence d'axe/montant
			
			// Check de présence champ classique
			$init_mod_champs = 'S=';
			$fin_nom_mod_champs = 'G=';
			$nb_carac_mod_champs_debut = strpos($formule_sans_reliures,$init_mod_champs); 
			

			if($nb_carac_montant_debut <> NULL) // Si on a un axe calculé ou montant conditionnel
			{
				$debut_axe_montant = strpos($formule_sans_reliures,'L=');
				$fin_axe_montant = strpos($formule_sans_reliures,'E=');
				$valeur_axe_montant = substr($formule_sans_reliures,$debut_axe_montant+2,($fin_axe_montant-3)-($debut_axe_montant)); 

				// Définition si axe calculé OU montant conditionnel
				$nature_axe_montant_debut = strpos($formule_sans_reliures,'Y=');
				$chaine_axe_montant = strstr($formule_sans_reliures,'Y=');
				$nature_axe_montant = substr($chaine_axe_montant,2,1);
				
				// Récupération de la formule d'un axe calculé
				
				if ($nature_axe_montant == '1') 
				{$nature_axe_montant = 'un axe calculé';
				// Recherche la formule de l'axe calculé	
				$debut_axe_formulax = strpos($formule_sans_reliures,'F=');
				$fin_axe_formulax = strpos($formule_sans_reliures,'Y=');
				$valeur_axe_formulax = substr($formule_sans_reliures,$debut_axe_formulax+2,($fin_axe_formulax-3)-($debut_axe_formulax)); 
				$nom_formulax = 'Formule de l\'axe calculé (N° = ID des champs): <strong>'.$valeur_axe_formulax.'</strong><br/><br/>';
			     } 
				elseif (($nature_axe_montant == '0')) 
				{$nature_axe_montant = 'un montant conditionnel'; 
				// Recherche la formule du montant conditionnel
				$debut_axe_formulax = strpos($formule_sans_reliures,'F=');
				$fin_axe_formulax = strpos($formule_sans_reliures,'Y=');
				$valeur_axe_formulax = substr($formule_sans_reliures,$debut_axe_formulax+2,($fin_axe_formulax-3)-($debut_axe_formulax)); 
				$nom_formulax = 'Formule du montant conditionnel (N° = ID des champs): <strong>'.$valeur_axe_formulax.'</strong><br/><br/>';}
				else {$nature_axe_montant = 'un Axe calculé / Montant conditionnel'; $valeur_axe_formulax = ''; $nom_formulax = '';}
				
				$nom_axe_montant = '<span class="titre_params_rubriques">Détails champ(s) :</span> <hr>
				C\'est '.$nature_axe_montant.' du nom de "<strong>'.$valeur_axe_montant.'</strong>" <br/><br/>';
				$nom_du_mod_champs =  '';
				$nom_source_mod_champs = '';
			}

			else // Sinon on récupère le champ normal
			{
				$nb_carac_mod_champs_fin = strpos($formule_sans_reliures,$fin_nom_mod_champs);
				$nom_mod_champs = substr($formule_sans_reliures,($nb_carac_mod_champs_debut+2), ($nb_carac_mod_champs_fin-$nb_carac_mod_champs_debut-3));
				
				if(is_numeric($nom_mod_champs)) // Débogage (si l'ID du champ est bien numérique, on applique ça)
				{
					// Requête pour récupérer le nom du champs et sa source
					$nom_reel_mod_champs = $bdd->query("
					SELECT TOP 1
					DESIGNATION as desig
					,SOURCE as source
					FROM
					DL_MODELES_CHAMPS
					WHERE
					ID_VUE = '".$nom_modele."' AND ID_DL = '".$nom_connecteur."' AND ID_MC = '".$nom_mod_champs."'
					");
					$nom_mod_champs_reel = $nom_reel_mod_champs->fetch();
					$vrai_nom_mod_champs = $nom_mod_champs_reel['desig'];
					$source_mod_champs = $nom_mod_champs_reel['source'];
					
					$nom_du_mod_champs = '<span class="titre_params_rubriques">Détails champ(s) :</span> <hr>
					Champ (axe) : "<strong>'.$vrai_nom_mod_champs.'</strong>" <em>(ID : "<strong>'.$nom_mod_champs.'</strong>")</em> <br/><br/>';
					$nom_source_mod_champs = ' Chemin table et champ : <strong>'.$source_mod_champs.'</strong><br/><br/>';
					$nom_axe_montant = '';
					$nom_formulax = '';
				}
				
				else // Sinon on vérifie que ce n'est pas un dossier virtuel, et si ce n'est pas le cas, message de débogage
				{
					$id_champ_mc_virtuel = strpos($nom_mod_champs, '|'); // Pour dossier virtuel
					if($id_champ_mc_virtuel <> NULL) // Si le champ est bien un dossier virtuel
					{
						$id_champ_mc_virtuel_dossier = substr($nom_mod_champs,0,$id_champ_mc_virtuel); // Récup de l'ID du champ virtuel
						$id_champ_mc_virtuel_champ = substr($nom_mod_champs,$id_champ_mc_virtuel+1,strlen($nom_mod_champs)); // Récup de l'ID MC du dossier virtuel
						// Requête pour récupérer le nom du champ virtuel
						$requete_nom_champ_virtuelle = $bdd->query("
								SELECT 
								  DL_MODELES_CHAMPS.DESIGNATION as desig
								  ,DL_MODELES_CHAMPS.SOURCE as source
								  ,DL_VUES.DESIGNATION as desig_vue
								  FROM DL_MODELES_CHAMPS
								  LEFT JOIN DL_VUES
								  ON
								  DL_VUES.ID_DL = DL_MODELES_CHAMPS.ID_DL 
								  AND
								  DL_VUES.ID_VUE = DL_MODELES_CHAMPS.ID_VUE
								  LEFT JOIN DL_VUES_VIRTUELLES
								  ON
								  DL_MODELES_CHAMPS.ID_DL = DL_VUES_VIRTUELLES.ID_DL
								  AND
								  DL_MODELES_CHAMPS.ID_VUE = DL_VUES_VIRTUELLES.ID_VUE_VIRTUELLE
								  WHERE 
								  DL_MODELES_CHAMPS.ID_MC = '".$id_champ_mc_virtuel_champ."'
								  AND
								  DL_MODELES_CHAMPS.ID_DL = '".$nom_connecteur."'
								  AND
								  DL_VUES_VIRTUELLES.ID = '".$id_champ_mc_virtuel_dossier."'
									");
						$fetch_nom_champ_virtuelle = $requete_nom_champ_virtuelle->fetch();
						$vrai_nom_mod_champs = $fetch_nom_champ_virtuelle['desig'];
						$source_mod_champs = $fetch_nom_champ_virtuelle['source'];
						$nom_vue_virtuelle = $fetch_nom_champ_virtuelle['desig_vue'];
						
						$nom_du_mod_champs = '<span class="titre_params_rubriques">Détails champ(s) :</span> <hr>
						Champ (axe) : "<strong>'.$vrai_nom_mod_champs.'</strong>" <em>(Champ virtualisé - ID : "<strong>'.$id_champ_mc_virtuel_champ.'</strong>" - Modèle : "<strong>'.$nom_vue_virtuelle.'</strong>")</em> <br/><br/>';
						$nom_source_mod_champs = ' Chemin table et champ virtuel : <strong>'.$source_mod_champs.'</strong><br/><br/>';
						$nom_axe_montant = '';
						$nom_formulax = ''; 
					}	
					else // Si ce n'est pas un champ normal ou virtuel, on inscrit le message d'erreur
					{
						if($vrai_nom_mod_champs = '')
						{
							$nom_du_mod_champs = 'Nom du champ introuvable :<em> Rapport =  ID MC is '.$vrai_nom_mod_champs.'</em><br><br>';
						}	
						
						else
						{
							$nom_du_mod_champs = 'Nom du champ introuvable :<em> Rapport =  l\'ID MC est incorrect (ni numérique ni virtualisé)</em><br><br>';
							$nom_source_mod_champs = '';
							$nb_de_champs_final = '';
							$nb_de_champs_ali = '';
							$nom_axe_montant = '';
							$nom_formulax = '';
						}	
						
					}	
				}	

				
			}
			

			$nb_carac_agreg_debut = strpos($formule_sans_reliures,'E='); // Check de présence agrégat
			
			if($nb_carac_agreg_debut <> NULL) // Si on a un agrégat
			{
				$debut_axe_agreg = strpos($formule_sans_reliures,'E=');
				$fin_axe_agreg = strpos($formule_sans_reliures,'S=');
				$valeur_axe_agreg = substr($formule_sans_reliures,$debut_axe_agreg+2,($fin_axe_agreg-3)-($debut_axe_agreg));
				
				// Définition de la nature de l'agrégat - isolation de sa valeur
				$nature_axe_agreg_debut = strpos($formule_sans_reliures,'E=');
				$chaine_axe_agreg = strstr($formule_sans_reliures,'E=');
				$nature_axe_agreg = substr($chaine_axe_agreg,2,1);
				if ($nature_axe_agreg == '0') {$nature_axe_agreg = 'Aucun agrégat';} 
				elseif (($nature_axe_agreg == '1')) {$nature_axe_agreg = 'Somme (total des valeurs)';} 
				elseif (($nature_axe_agreg == '2')) {$nature_axe_agreg = 'Compter (nombre de valeurs)';} 
				elseif (($nature_axe_agreg == '3')) {$nature_axe_agreg = 'Moyenne (moyenne des valeurs)';} 
				elseif (($nature_axe_agreg == '4')) {$nature_axe_agreg = 'Minimum (affiche la valeur la plus basse)';} 
				elseif (($nature_axe_agreg == '5')) {$nature_axe_agreg = 'Maximum (affiche la valeur la plus haute)';}
				elseif (($nature_axe_agreg == '6')) {$nature_axe_agreg = 'Somme distincte (total distinct des valeurs)';} 
				elseif (($nature_axe_agreg == '7')) {$nature_axe_agreg = 'Moyenne distincte (moyenne distincte des valeurs)';} 
				elseif (($nature_axe_agreg == '8')) {$nature_axe_agreg = 'Compter distinct (total distinct des valeurs)';} 
				else {$nature_axe_agreg = 'agrégat inconnu';}
				$nom_axe_agreg = 'Agrégat : <strong>'.$nature_axe_agreg.'</strong><br/><br/>';
				
			}
			
			else 
			{
				$nom_axe_agreg = '';
			}
			
			$nb_carac_filtres_debut = strpos($formule_sans_reliures,'@R'); // Check de présence filtres - A CORRIGER pour filtres avancées etc.
			
			// Mise en place pour filtres avec reF. cellule (s'il y a)
			$chaine_filtres_mep = strstr($formule_sans_reliures,'@R'); // Précaution pour commencer la boucle des ref. cellule à la fin de la formule
			$chaine_ref_cellule_mep = strstr($chaine_filtres_mep,'";');
			$fin_chaine_ref_cellule = strpos($chaine_ref_cellule_mep, ')');
			$chaine_ref_cellule_brut = substr($chaine_ref_cellule_mep,strlen($chaine_ref_cellule_mep)-$fin_chaine_ref_cellule);
			$chaine_ref_cellule = str_replace(')','',str_replace('"','',$chaine_ref_cellule_brut));
			if(strlen($chaine_ref_cellule) > 0) // S'il y a bien des ref aux cellules dans les filtres
			{
				$nb_de_ref_cellule = substr_count($chaine_ref_cellule,';');
				$q =1;
				while ($q <= $nb_de_ref_cellule) // Boucle pour segmenter les ref. cellules
				{
					if($q === 1) // Si on est sur la première ref, on définit la première chaine
					{
					${'cellule_ref_brut'.$q} = $chaine_ref_cellule;
					}	
					else
					{
						${'cellule_ref_brut'.$q} = substr(${'cellule_ref_sans_fin'.($q-1)},strpos(${'cellule_ref_sans_fin'.($q-1)},';'));
					}
				
					${'cellule_ref_sans_fin'.$q} = substr(${'cellule_ref_brut'.$q},1);
					${'cellule_ref_fin'.$q} = strpos(${'cellule_ref_sans_fin'.$q},';');
					if(${'cellule_ref_fin'.$q} <> NULL)
					{	
						${'cellule_ref'.$q} = substr(${'cellule_ref_sans_fin'.$q},0,${'cellule_ref_fin'.$q});
					}
					else
					{
						${'cellule_ref'.$q} = ${'cellule_ref_sans_fin'.$q};
					}
					
					if(strlen(${'cellule_ref'.$q}) >= 4 AND substr_count(${'cellule_ref'.$q},'$') === 2) // Si la ref. cellule est invariante
					{
						${'cellule_ref_filtres'.$q} = 'Cellule '.str_replace('$','',${'cellule_ref'.$q}). ' (Invariant)';
					}	
					elseif(substr(${'cellule_ref'.$q},0,1) === '$' AND substr_count(${'cellule_ref'.$q},'$') === 1) // Si la ref. cellule est variante en ligne uniquement
					{	
						${'cellule_ref_filtres'.$q} = 'Cellule '.str_replace('$','',${'cellule_ref'.$q}). ' (Variant en ligne)';
					}
					elseif(substr_count(${'cellule_ref'.$q},'$') === 0) // Si la ref. cellule est variante en colonnes uniquement
					{
						${'cellule_ref_filtres'.$q} = 'Cellule '.str_replace('$','',${'cellule_ref'.$q}). ' (Variant en ligne et en colonne)';
					}	
					else
					{
						${'cellule_ref_filtres'.$q} = 'Cellule '.str_replace('$','',${'cellule_ref'.$q}). ' (Variant en colonne)';
					}	
					$q++;
				}	
			}
			
			if($nb_carac_filtres_debut <> NULL) // Si on a des filtres
			{
				$debut_axe_filtres = strpos($formule_sans_reliures,'@R');
				$iresence_filtres_avancees = strpos(strstr($formule_sans_reliures,'@R'), ':@'); // Check présence filtre avancés
				
				if ($iresence_filtres_avancees <> NULL) // Si on a des filtres avancés ou non, on adapte la fin axe filtres
				{
					$fin_axe_filtres = strpos($formule_sans_reliures,':@')+1; // Fin de la formule
				}
				else
				{
					$fin_axe_filtres = strpos($formule_sans_reliures,':"')+1; // Fin de la formule
				}	
				
				$valeur_axe_filtres = substr($formule_sans_reliures,$debut_axe_filtres+1,(($fin_axe_filtres)-1)-($debut_axe_filtres));
				$nb_de_filtres = mb_substr_count($valeur_axe_filtres, 'R='); // Compte le nombre de filtres
		
					$i = 1;
					while ($i <= $nb_de_filtres) // Boucle pour segmetner les autres chaines de filtres (s'il y a)
					{
						if($i === 1) // Si on est au premier filtre, le traitement est différent
						{
							// Ordre du filtres
					
							${'chaine_filtre_global'.$i} = strstr($formule_sans_reliures,'R=');
							${'debut_filtre'.$i} = strpos(${'chaine_filtre_global'.$i},'R');
							${'fin_filtre'.$i} = strpos(${'chaine_filtre_global'.$i},':');
							${'chaine_filtre'.$i} = substr(${'chaine_filtre_global'.$i}, ${'debut_filtre'.$i}, ${'fin_filtre'.$i});
							
							// Recup des arguments du premier filtre : 
								
							${'chaine_champ_filtre'.$i} = substr(${'chaine_filtre'.$i},strpos(${'chaine_filtre'.$i},'S')+2,(strpos(${'chaine_filtre'.$i},'V'))-7);
						}	
						
						else
						{
							// Définition chaine de chaque filtre
							${'chaine_filtre_global'.$i} = substr(strstr(${'chaine_filtre_global'.($i-1)},':'),1,strlen(strstr(${'chaine_filtre_global'.($i-1)},':')));
							${'debut_filtre'.$i} = 0;
							${'fin_filtre'.$i} = strpos(${'chaine_filtre_global'.$i},':');
							${'chaine_filtre'.$i} = substr(${'chaine_filtre_global'.$i},${'debut_filtre'.$i},${'fin_filtre'.$i});
						}	
						
						// Définition des arguments des filtres
						
						${'chaine_champ_filtre'.$i} = substr(${'chaine_filtre'.$i},strpos(${'chaine_filtre'.$i},'S')+2,(strpos(${'chaine_filtre'.$i},'V'))-7);
						
						if(is_numeric(${'chaine_champ_filtre'.$i} )) // Débogage
						{
						
						// Requête pour récupérer le champ du filtre
						${'nom_reel_filtre_champ'.$i} = $bdd->query("
						SELECT TOP 1
						DESIGNATION as desig
						FROM
						DL_MODELES_CHAMPS
						WHERE
						ID_VUE = '".$nom_modele."' AND ID_DL = '".$nom_connecteur."' AND ID_MC = '".${'chaine_champ_filtre'.$i}."'
						");
						
						${'nom_filtre_champ_reel'.$i} = ${'nom_reel_filtre_champ'.$i}->fetch();
						${'nom_filtre_champ'.$i} = ${'nom_filtre_champ_reel'.$i}['desig'];
						
						// Récup de la valeur du filtre
						
						${'recup_valeur_filtre'.$i} = strstr(${'chaine_filtre'.$i},'V=');
						${'valeur_filtre'.$i} = substr(${'recup_valeur_filtre'.$i},2);
						
						}
						
						else
						{
							${'chaine_champ_filtre_virtuel'.$i} = strpos(${'chaine_champ_filtre'.$i}, '|'); // Pour dossier virtuel
							if(${'chaine_champ_filtre_virtuel'.$i} <> NULL) // Si le champ est bien un dossier virtuel
							{
								${'chaine_champ_filtre_virtuel_dossier'.$i} = substr(${'chaine_champ_filtre'.$i},0,${'chaine_champ_filtre_virtuel'.$i}); // Récup de l'ID du dossier virtuel
								${'chaine_champ_filtre_champ'.$i} = substr(${'chaine_champ_filtre'.$i},${'chaine_champ_filtre_virtuel'.$i}+1,strlen(${'chaine_champ_filtre'.$i})); // Récup de l'ID MC du dossier virtuel
								// Requête pour récupérer le nom du champ virtuel
								${'requete_champ_filtre_virtuelle'.$i} = $bdd->query("
										SELECT 
										  DL_MODELES_CHAMPS.DESIGNATION as desig
										  ,DL_VUES.DESIGNATION as desig_vue
										  FROM DL_MODELES_CHAMPS
										  LEFT JOIN DL_VUES
										  ON
										  DL_VUES.ID_DL = DL_MODELES_CHAMPS.ID_DL 
										  AND
										  DL_VUES.ID_VUE = DL_MODELES_CHAMPS.ID_VUE
										  LEFT JOIN DL_VUES_VIRTUELLES
										  ON
										  DL_MODELES_CHAMPS.ID_DL = DL_VUES_VIRTUELLES.ID_DL
										  AND
										  DL_MODELES_CHAMPS.ID_VUE = DL_VUES_VIRTUELLES.ID_VUE_VIRTUELLE
										  WHERE 
										  DL_MODELES_CHAMPS.ID_MC = '".${'chaine_champ_filtre_champ'.$i}."'
										  AND
										  DL_MODELES_CHAMPS.ID_DL = '".$nom_connecteur."'
										  AND
										  DL_VUES_VIRTUELLES.ID = '".${'chaine_champ_filtre_virtuel_dossier'.$i}."'
											");
								${'fetch_champ_filtre_virtuelle'.$i} = ${'requete_champ_filtre_virtuelle'.$i}->fetch();
								${'nom_filtre_champ_remontee'.$i} = ${'fetch_champ_filtre_virtuelle'.$i}['desig'];
								${'nom_vue_virtuelle'.$i} = ${'fetch_champ_filtre_virtuelle'.$i}['desig_vue'];
								${'recup_valeur_filtre'.$i} = strstr(${'chaine_filtre'.$i},'V=');
								${'valeur_filtre'.$i} = substr(${'recup_valeur_filtre'.$i},2);
								
								if(strlen(${'chaine_champ_filtre_champ'.$i}) === 0 XOR strlen(${'nom_filtre_champ_remontee'.$i}) === 0)
								{
									${'nom_filtre_champ'.$i} = 'Filtre '.$i.' inconnu (virtualisé - pas sur la base -<em> ID remonté : '.${'chaine_champ_filtre'.$i}.'</em>)';
								}

								else
								{	
									${'nom_filtre_champ'.$i} = ${'nom_filtre_champ_remontee'.$i}. '<em></strong> (Champ virtualisé - Modèle d\'origine : '.${'nom_vue_virtuelle'.$i}.')</em>';
								}
							}
							else // Si ce n'est pas un dossier virtuel
							{
									${'nom_filtre_champ'.$i} = 'Filtre '.$i.' inconnu (ID retrouvé : '.${'chaine_champ_filtre_champ'.$i}.') - Valeur : "'.${'valeur_filtre'.$i}.'"<br>';
							}	
						}		
						
						// Mise en place pour retrouver les références aux cellules que font les filtres (si référence il y a)
						${'accolade_filtre_debut'.$i} = strpos(${'valeur_filtre'.$i},'{');
						${'accolade_filtre_fin'.$i} = strpos(${'valeur_filtre'.$i},'}');
						
						if(is_numeric(${'accolade_filtre_debut'.$i}) AND is_numeric(${'accolade_filtre_fin'.$i})) // Si on a bien une ref. à un cellule du classeur
							{
								$s = (str_replace('}','',(str_replace('{','',${'valeur_filtre'.$i})))+1); // Trouver le numéro ref de la cellule
								${'valeur_filtre'.$i} = ${'cellule_ref_filtres'.$s}; // On fait référence à la ref défini plus haut
							}	
				
						// Définition finale du filtre
						
						if($i === 1) // Si on est au premier filtre, le traitement est différent  (on ajout l'en-tête du paragraphe filtre)
						{
							${'nom_du_filtre'.$i} = '<span class="titre_params_rubriques">Détails filtre(s) :</span> <hr>
							Filtre sur : <strong>'.${'nom_filtre_champ'.$i}.'</strong> - Valeur : "'.${'valeur_filtre'.$i}.'"<br/><br/>';	
						}
						else
						{
							${'nom_du_filtre'.$i} = 'Filtre sur : <strong>'.${'nom_filtre_champ'.$i}.'</strong> - Valeur : "'.${'valeur_filtre'.$i}.'"<br/><br/>';
						}	
						$i++;

					}
				
				if ($iresence_filtres_avancees <> NULL) // Si on a des filtres avancés ou non 
				{
					$chaine_filtres_avances_base = strstr(str_replace('"','',$valeur_axe_filtres), ':@');
					$chaine_filtres_avances = substr($chaine_filtres_avances_base,2);
					$filtres_avances = 'Filtres avancés : "'.$chaine_filtres_avances. '"<br/<br/>';
				}	
				
				else
				{
					$filtres_avances = '';
				}
				
				$nb_de_champs_ali = 0;
				$nb_de_champs_final = '';
				$nom_detail_champ = '';
				$nb_de_champs_acu = 0;
				$nb_de_caches = '';
				$nb_de_caches_numeric = 0;
				
			}
			
			else
			{
					$nb_de_champs_ali = 0;
					$nb_de_filtres = 1;
					$nom_du_filtre1 = '';
					$nb_de_champs_final = '';
					$filtres_avances = '';
					$nb_de_champs_acu = 0;
					$nb_de_caches = '';
					$nb_de_caches_numeric = 0;
			}	

				

		}
		
		elseif (substr($_POST['formule'],5,2) == 'AL' XOR (substr($_POST['formule'],6,2) == 'AL')) // Définition des arguments à suivre pour un assistant liste (voir la page incluse)
		{
		include("ali_avec_bdd.php");
		}	
		
		elseif (substr($_POST['formule'],5,2) == 'SR' XOR (substr($_POST['formule'],6,2) == 'SR')) // Définition des arguments à suivre pour un assistant cube (voir la page incluse)
		{
		include("acu_avec_bdd.php");
		}	
		
		elseif (substr($_POST['formule'],5,2) == '.E') // Définition des arguments à suivre pour du log In-Memory (voir la page incluse)
		{
		include("in_memory.php");
		}	


		else // Si ce n'est pas un assistant ou un log(?)
		{
			$nom_axe_montant = '';
			$nom_du_mod_champs =  '';
			$nom_source_mod_champs = '';
			$nom_axe_agreg = '';
			$nom_formulax = '';
			$nom_axe_filtres = '';
			$nb_de_filtres = 1;
			$nom_du_filtre1 = '';
			$filtres_avances = '';
			$nb_de_champs_ali = 0;
			$nb_de_champs_final = '';
			$nom_detail_champ = '';
			$nb_de_champs_acu = 0;
			$nb_de_caches = '';
			$nb_de_caches_numeric = 0;
		}
		
		// Informations diffusées :
		
		echo '<span class="titre_params">Résultat : </span><br/><br/>
		<hr>
		<span class="titre_params_rubriques">Nature :</span>
		<hr>
		Assistant : <strong>'.$type_assistant.'</strong> <br/><br/>'
		.$nom_du_connecteur.
		$nom_du_modele.
		$nom_du_mod_champs.
		$nom_source_mod_champs.
		$nb_de_champs_final.
		$nb_de_caches;
		
		$m=1;
		while ($m <= $nb_de_caches_numeric) // Boucle pour segmenter les caches (In-Memory)
		{
				echo ${'detail_cache'.$m};
				$m++;
		}	
		
		
		$k=1;
		while ($k <= $nb_de_champs_ali) // Boucle pour segmenter les champs (ALI)
		{
				echo ${'nom_detail_champ'.$k};
				$k++;
		}	
		
		$g=1;
		while ($g <= $nb_de_champs_acu) // Boucle pour segmenter les champs (ACU etc)
		{
				echo ${'nom_detail_champ'.$g};
				$g++;
		}	
		
		echo
		$nom_axe_montant.
		$nom_formulax.
		$nom_axe_agreg;
		
		$j=1;
		while ($j <= $nb_de_filtres) // Boucle pour segmenter les filtres (s'il y a)
		{
				echo ${'nom_du_filtre'.$j};
				$j++;
		}	
		
		echo
		$filtres_avances.
		//$valeur_axe_formulax2;
		// ' - '.
		// $chaine_calculee_pre72.
		// $champ_axe_calcule2.
		// $formule_sans_reliures;
		// $nombre_colonnes.' --- <br> '.	
		// $nombre_lignes.' --- <br> '.	
		// $valeur_axe_filtres.' --- <br> '.	
		// $formule_sans_reliures.' --- <br> '.	
		'
		</article>';
		// REVOIR FORMULE TYPE 'NOM FORMULAX' QUI N'A PAS A ETRE DIFFERENTES DES AUTRES VARIABLES DE CHAMPS
?>