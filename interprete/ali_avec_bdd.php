<?php

// Définition des champs
$formule_sans_reliures = str_replace('"&"','',$_POST['formule']);
$formule_sans_reliures = str_replace('=@RI','=RI',$formule_sans_reliures); // Pour gérer les nouvelles formules =@
$chaine_champ_sans_fin_avec_filtres = strstr($formule_sans_reliures,'@');
$longueur_chaine_sans_fin = strlen($chaine_champ_sans_fin_avec_filtres);
$chaine_champ_rework_sans_arobase_debut = substr($chaine_champ_sans_fin_avec_filtres,1,$longueur_chaine_sans_fin);
$position_chaine_champ_sans_fin = strpos($chaine_champ_rework_sans_arobase_debut,'@');

if($position_chaine_champ_sans_fin <> NULL) // Si on a bien des filtres
{
	$chaine_champs = substr($chaine_champ_rework_sans_arobase_debut,0,$position_chaine_champ_sans_fin); // On génère la partie de la formule sur les champs en prenant en compte les filtres en fin de formule HIHI
}
else
{
	$position_chaine_champ_sans_fin = strrpos($chaine_champ_rework_sans_arobase_debut,':'); // On redéfinie la position finale
	$chaine_champs = substr($chaine_champ_rework_sans_arobase_debut,0,$position_chaine_champ_sans_fin+1);  // On génère la partie de la formule sur les champs sans filtre pris en compte
}	

// Création d'une boucle pour chaque champ

// DEFINITION CHAINE DE CHAQUE CHAMP DE L'ALI
		
$nb_de_champs_ali = mb_substr_count($chaine_champs, ':'); // Compte le nombre de champs dans l'ALI
$nb_de_champs_final = '<span class="titre_params_rubriques">Détails champ(s) :</span> <hr> Nombre de champ(s) : <strong>'.$nb_de_champs_ali.'</strong><br/><br/>';

$i = 1;  // Boucle pour segmenter les chaines de champs ALI
while ($i <= $nb_de_champs_ali)
{
	if($i === 1) // Traitement différent pour le 1er champ
	{
		${'chaine_champ_ali_global'.$i} = $chaine_champs;
		${'debut_champ_ali'.$i} = 0;
		${'fin_champ_ali'.$i} = strpos(${'chaine_champ_ali_global'.$i},':')+1;
		${'chaine_champ_ali'.$i} = substr(${'chaine_champ_ali_global'.$i},${'debut_champ_ali'.$i},${'fin_champ_ali'.$i});
	}
	
	else
	{
	${'chaine_champ_ali_global'.$i} = substr(${'chaine_champ_ali_global'.($i-1)},(strlen(${'chaine_champ_ali_global'.($i-1)})-strlen(strstr(${'chaine_champ_ali_global'.($i-1)},':')))+1,strlen(${'chaine_champ_ali_global'.($i-1)}));
	${'debut_champ_ali'.$i} = 0;
	${'fin_champ_ali'.$i} = strpos(${'chaine_champ_ali_global'.$i},':');
	${'chaine_champ_ali'.$i} = substr(${'chaine_champ_ali_global'.$i},${'debut_champ_ali'.$i},${'fin_champ_ali'.$i}+1);
	}
	
	// Récup de l'agreg du champ 2++
	${'check_agreg'.$i} = strpos(${'chaine_champ_ali'.$i},'L'); // Si axe calculé, on ramène l'agrégat différemment
	if(is_numeric(${'check_agreg'.$i})) // S'il y a bien un axe calculé
	{
		${'longueur_difference_agreg'.$i} = (strpos(${'chaine_champ_ali'.$i},'G=')-1)-(strpos(${'chaine_champ_ali'.$i},'E=')+2);
	}	
	else // Sinon on ramène l'agragat de manière classique
	{
	${'longueur_difference_agreg'.$i} = (strpos(${'chaine_champ_ali'.$i},'S=')-1)-(strpos(${'chaine_champ_ali'.$i},'E=')+2);
	}
	${'valeur_agreg_champ_check'.$i} = substr(${'chaine_champ_ali'.$i},strpos(${'chaine_champ_ali'.$i},'E=')+2,${'longueur_difference_agreg'.$i});
	
	${'valeur_agreg_champ'.$i} = ${'valeur_agreg_champ_check'.$i};
	
	if (${'valeur_agreg_champ'.$i} == '0') {${'valeur_agreg_champ'.$i} = 'Aucun';} 
	elseif (${'valeur_agreg_champ'.$i} == '1') {${'valeur_agreg_champ'.$i} = 'Somme';} 
	elseif (${'valeur_agreg_champ'.$i} == '2') {${'valeur_agreg_champ'.$i} = 'Compter';} 
	elseif (${'valeur_agreg_champ'.$i} == '3') {${'valeur_agreg_champ'.$i} = 'Moyenne';} 
	elseif (${'valeur_agreg_champ'.$i} == '4') {${'valeur_agreg_champ'.$i} = 'Maximum';} 
	elseif (${'valeur_agreg_champ'.$i} == '5') {${'valeur_agreg_champ'.$i} = 'Minimum';}
	elseif (${'valeur_agreg_champ'.$i} == '6') {${'valeur_agreg_champ'.$i} = 'Somme distincte';} 
	elseif (${'valeur_agreg_champ'.$i} == '7') {${'valeur_agreg_champ'.$i} = 'Moyenne distincte';} 
	elseif (${'valeur_agreg_champ'.$i} == '8') {${'valeur_agreg_champ'.$i} = 'Compter distinct';} 
	elseif (${'valeur_agreg_champ'.$i} == '9') {${'valeur_agreg_champ'.$i} = 'Somme non-totalisée';} 
	elseif (${'valeur_agreg_champ'.$i} == '10') {${'valeur_agreg_champ'.$i} = 'Compter non-totalisé';} 
	elseif (${'valeur_agreg_champ'.$i} == '11') {${'valeur_agreg_champ'.$i} = 'Moyenne non-totalisée';} 
	else {${'valeur_agreg_champ'.$i} = 'agrégat inconnu (ID retrouvé : '.${'valeur_agreg_champ_check'.$i} .')';}
	${'nom_agreg_champ'.$i} = ' - Agrégat : <strong>'.${'valeur_agreg_champ'.$i}.'</strong>';
	
	// Récup du nom MC du champ 2++
	
	// Vérification si axe calculé / Montant conditionnel

	${'check_axe'.$i} = strpos(${'chaine_champ_ali'.$i}, 'Y=');
	if(${'check_axe'.$i} <> NULL)
	{
		${'debut_axe_montant'.$i} = strpos(${'chaine_champ_ali'.$i},'L=');
		${'fin_axe_montant'.$i} = strpos(${'chaine_champ_ali'.$i},'E=');
		${'valeur_axe_montant'.$i} = substr(${'chaine_champ_ali'.$i},${'debut_axe_montant'.$i} +2,(${'fin_axe_montant'.$i}-3)-(${'debut_axe_montant'.$i} )); 

		// Définition si axe calculé OU montant conditionnel
		${'nature_axe_montant_debut'.$i} = strpos(${'chaine_champ_ali'.$i},'Y=');
		${'chaine_axe_montant'.$i} = strstr(${'chaine_champ_ali'.$i},'Y=');
		${'nature_axe_montant'.$i}= substr(${'chaine_axe_montant'.$i},2,1);
					
		// Récupération de la formule d'un axe calculé
					
					if (${'nature_axe_montant'.$i} == '1') // Si axe calculé
					{
						${'nature_axe_montant'.$i} = '<strong>Axe calculé</strong>';
						// Recherche la formule de l'axe calculé	
						${'debut_axe_formulax'.$i}  = strpos(${'chaine_champ_ali'.$i},'F=');
						${'fin_axe_formulax'.$i}  = strpos(${'chaine_champ_ali'.$i},'Y=');
						${'valeur_axe_formulax'.$i}  = substr(${'chaine_champ_ali'.$i},${'debut_axe_formulax'.$i} +2,(${'fin_axe_formulax'.$i} -3)-(${'debut_axe_formulax'.$i} ));
						
						// ==== TEST RECUP CHAMPS AXE CALCULÉ  ============================
						
						$nb_de_champs_calcules = substr_count(${'valeur_axe_formulax'.$i},'['); // On compte le nombre de champs utilisé
						if($nb_de_champs_calcules === 0){${'nom_formulax'.$i} = '<strong>'.${'valeur_axe_formulax'.$i}.'</strong>';} // Si pas de [champs], on renvoi la formule telle quelle
						$x=1;
						while ($x <= $nb_de_champs_calcules) // Boucle pour segmenter les champs de l'axe calculé
						{
							if($x === 1)
							{	
								${'chaine_calculee_pre'.$i.$x} = ${'valeur_axe_formulax'.$i}; // Si on est au premier champ, on prend la formule en entier
							}
							else
							{
								${'chaine_calculee_pre'.$i.$x} = substr(${'chaine_calculee_pre'.$i.($x-1)},(${'ref_axe_fin'.$i.($x-1)}+1),9999999); // Sinon on prend la formule transformé -1 et on répète le process précédent depuis cette chaine
							}
							
							${'ref_axe_debut'.$i.$x} = strpos(${'chaine_calculee_pre'.$i.$x},'['); // Trouver le prochain champ exactement (debut, puis fin).
							${'ref_axe_fin'.$i.$x} = strpos(${'chaine_calculee_pre'.$i.$x},']');
							${'champ_axe_calcule'.$i.$x} =  substr(${'chaine_calculee_pre'.$i.$x},${'ref_axe_debut'.$i.$x},(${'ref_axe_fin'.$i.$x}-${'ref_axe_debut'.$i.$x}+1)); // On isole le [Champ]
							${'champ_axe_calcule'.$i.$x} = substr(${'champ_axe_calcule'.$i.$x},1,-1); // On ramène l'intérieur du [Champ] (soit "Champ")
								
							if(is_numeric(${'champ_axe_calcule'.$i.$x}) OR substr_count(${'champ_axe_calcule'.$i.$x},'|') === 1) // Si la valeur du [Champ] est numérique ou virtualisé, on le cherche en BDD et on boucle pour chaque champ détecté
							{
										if(is_numeric(${'champ_axe_calcule'.$i.$x}))
										{	
											// Requête pour récupérer le nom du champ
											${'requete_nom_champ_axe'.$i.$x} = $bdd->query("
															SELECT TOP 1
															ID_MC as id_mc
															,DESIGNATION as desig
															,SOURCE as source
															FROM
															DL_MODELES_CHAMPS
															WHERE
															ID_VUE = '".$nom_modele."' AND ID_DL = '".$nom_connecteur."' AND ID_MC = '".${'champ_axe_calcule'.$i.$x}."'
															");
											${'fetch_nom_champ_axe'.$i.$x} = ${'requete_nom_champ_axe'.$i.$x}->fetch();
											${'id_champ_axe'.$i.$x} = ${'fetch_nom_champ_axe'.$i.$x}['id_mc'];
											${'nom_champ_axe'.$i.$x} = ${'fetch_nom_champ_axe'.$i.$x}['desig'];
											${'source_champ_axe'.$i.$x} = ${'fetch_nom_champ_axe'.$i.$x}['source'];
											
											if(strlen(${'nom_champ_axe'.$i.$x}) === 0) // Si la requête ne ramène rien, on ramène un message d'erreur
											{
												${'id_champ_axe'.$i.$x} = ${'champ_axe_calcule'.$i.$x};
												${'nom_champ_axe'.$i.$x} = '<a class="red" title="Champ introuvable ! L\'ID affiché n\'existe pas dans le dictionnaire." rel="tooltip">['.${'id_champ_axe'.$i.$x}.']</a>';
												if($x === 1) // Traitement si 1er champ de l'axe/montant cond., et qu'on a pas de nom de champ de trouvé dans le dico
												{
													${'chaine_calculee'.$i.$x} = ' <strong>'.str_replace('['.${'champ_axe_calcule'.$i.$x}.']',${'nom_champ_axe'.$i.$x},${'chaine_calculee_pre'.$i.$x}).'</strong></em>';
												}
												else // Traitement si PAS le 1er champ de l'axe/montant cond., et qu'on a pas de nom de champ de trouvé dans le dico
												{
													${'chaine_calculee'.$i.$x} = ' <strong>'.str_replace('['.${'champ_axe_calcule'.$i.$x}.']',${'nom_champ_axe'.$i.$x},${'nom_formulax'.$i.($x-1)}).'</strong></em>';
												}
											}
											
											else // Si on a bien un résultat, on le remonte avec sa source et l'ID d'origine
											{
												if($x === 1) // Traitement différent si 1ere occurence de champ dans l'axe/montant
												{
													${'chaine_calculee'.$i.$x} = ' <strong>'.str_replace('['.${'champ_axe_calcule'.$i.$x}.']','<a title="<strong>ID du champ :</strong> '.${'id_champ_axe'.$i.$x}.'<br><strong>Source :</strong> '.str_replace('"','',${'source_champ_axe'.$i.$x}).' " rel="tooltip">['.${'nom_champ_axe'.$i.$x}.']</a>',${'chaine_calculee_pre'.$i.$x}).'</strong></em>';
												}
												else // Traitement si PAS la 1ere occurence de champ dans l'axe/montants
												{
													${'chaine_calculee'.$i.$x} = ' <strong>'.str_replace('['.${'champ_axe_calcule'.$i.$x}.']','<a title="<strong>ID du champ :</strong> '.${'id_champ_axe'.$i.$x}.'<br><strong>Source :</strong> '.str_replace('"','',${'source_champ_axe'.$i.$x}).' " rel="tooltip">['.${'nom_champ_axe'.$i.$x}.']</a>',${'nom_formulax'.$i.($x-1)}).'</strong></em>';
												}
											}
										}	
										
										elseif(substr_count(${'champ_axe_calcule'.$i.$x},'|') === 1) // Si l'ID_MC récupéré est un ID de dossier virtuel
										{
											${'id_champ_mc_virtuel_axe'.$i.$x} = strpos(${'champ_axe_calcule'.$i.$x}, '|'); // Pour dossier virtuel
											${'id_champ_mc_virtuel_dossier_axe'.$i.$x} = substr(${'champ_axe_calcule'.$i.$x},0,${'id_champ_mc_virtuel_axe'.$i.$x}); // Récup de l'ID du dossier virtuel
											${'id_champ_mc_virtuel_champ_axe'.$i.$x} = substr(${'champ_axe_calcule'.$i.$x},${'id_champ_mc_virtuel_axe'.$i.$x}+1,strlen(${'champ_axe_calcule'.$i.$x})); // Récup de l'ID MC du dossier virtuel
											//Requête pour récupérer le nom du champ virtuel
											${'requete_nom_champ_virtuelle'.$i.$x} = $bdd->query("
													SELECT 
													  DL_MODELES_CHAMPS.ID_MC as id_mc
													  ,DL_MODELES_CHAMPS.SOURCE as source
													  ,DL_MODELES_CHAMPS.DESIGNATION as desig
													  ,DL_VUES.DESIGNATION as desig_vue
													  FROM DL_MODELES_CHAMPS
													  LEFT JOIN DL_VUES
													  ON
													  DL_VUES.ID_DL = DL_MODELES_CHAMPS.ID_DL 
													  AND
													  DL_VUES.ID_VUE = DL_MODELES_CHAMPS.ID_VUE
													  LEFT JOIN DL_VUES_VIRTUELLES
													  ON
													  DL_MODELES_CHAMPS.ID_DL = DL_VUES_VIRTUELLES.ID_DL
													  AND
													  DL_MODELES_CHAMPS.ID_VUE = DL_VUES_VIRTUELLES.ID_VUE_VIRTUELLE
													  WHERE 
													  DL_MODELES_CHAMPS.ID_MC = '".${'id_champ_mc_virtuel_champ_axe'.$i.$x}."'
													  AND
													  DL_MODELES_CHAMPS.ID_DL = '".$nom_connecteur."'
													  AND
													  DL_VUES_VIRTUELLES.ID = '".${'id_champ_mc_virtuel_dossier_axe'.$i.$x}."'
														");
											${'fetch_nom_champ_virtuelle_axe'.$i.$x} = ${'requete_nom_champ_virtuelle'.$i.$x}->fetch();
											${'id_champ_axe'.$i.$x} = ${'fetch_nom_champ_virtuelle_axe'.$i.$x}['id_mc'];
											${'nom_champ_axe'.$i.$x} = ${'fetch_nom_champ_virtuelle_axe'.$i.$x}['desig'];
											${'source_champ_axe'.$i.$x} = ${'fetch_nom_champ_virtuelle_axe'.$i.$x}['source'];
											${'nom_vue_virtuelle_axe'.$i.$x} = ${'fetch_nom_champ_virtuelle_axe'.$i.$x}['desig_vue'];
											
											if(strlen(${'nom_champ_axe'.$i.$x}) === 0) // Si la requête du champ virtuel n'a rien ramenée
											{
												${'id_champ_axe'.$i.$x} = ${'champ_axe_calcule'.$i.$x};
												${'nom_champ_axe'.$i.$x} = '<a class="red" title="Champ VIRTUEL introuvable ! L\'ID affiché n\'existe pas dans le dictionnaire." rel="tooltip">['.${'id_champ_axe'.$i.$x}.']</a>';
												if($x === 1) // Si la requête du champ virtuel n'a rien ramenée + 1er champ de l'axe/montant : traitement différent
												{
													${'chaine_calculee'.$i.$x} = ' <strong>'.str_replace('['.${'champ_axe_calcule'.$i.$x}.']',${'nom_champ_axe'.$i.$x} ,${'chaine_calculee_pre'.$i.$x}).'</strong></em>';
												}
												else // Si la requête du champ virtuel n'a rien ramenée + PAS le 1er champ de l'axe/montant
												{
													${'chaine_calculee'.$i.$x} = ' <strong>'.str_replace('['.${'champ_axe_calcule'.$i.$x}.']',${'nom_champ_axe'.$i.$x} ,${'nom_formulax'.$i.($x-1)}).'</strong></em>';
												}
											}
											
											else // Si la requête avec ID_MC en dossier virtuel a bien ramené une valeur
											{
												if($x === 1) // Traitement différent si 1er champ de l'axe/montant
												{
													${'chaine_calculee'.$i.$x} = ' <strong>'.str_replace('['.${'champ_axe_calcule'.$i.$x}.']','<a class="virtual" title="<strong>ID du champ :</strong> '.${'id_champ_axe'.$i.$x}.'<br><strong>Source (VIRTUELLE) :</strong> '.str_replace('"','',${'source_champ_axe'.$i.$x}).' " rel="tooltip">'.'['.${'nom_champ_axe'.$i.$x}.']'.'</a>',${'chaine_calculee_pre'.$i.$x}).'</strong></em>';
												}
												else // Traitement si PAS le 1er champ de l'axe/montant
												{
													${'chaine_calculee'.$i.$x} = ' <strong>'.str_replace('['.${'champ_axe_calcule'.$i.$x}.']','<a class="virtual" title="<strong>ID du champ :</strong> '.${'id_champ_axe'.$i.$x}.'<br><strong>Source (VIRTUELLE) :</strong> '.str_replace('"','',${'source_champ_axe'.$i.$x}).' " rel="tooltip">'.'['.${'nom_champ_axe'.$i.$x}.']'.'</a>',${'nom_formulax'.$i.($x-1)}).'</strong></em>';
												}
												}
				
										}	
											
										else // Débug : si ce n'est pas un ID_MC classique ou virtuel, mais qu'on l'a détecté comme étant numérique							
										{
											${'champ_axe_calcule'.$i.$x} = ${'id_champ_axe'.$i.$x};
											${'nom_champ_axe'.$i.$x} = 'Champ axe '.$i.$x.' inconnu (pas sur la base - ID : '.${'id_champ_axe'.$i.$x}.')';
										}								
							}
							
							else // Si la valeur du champ de la boucle de l'axe/montant cond. n'a pas été détecté comme numérique, on suppose que c'est une réf. à un autre axe calculé/montant cond.. 
							{
								
								${'nom_champ_axe'.$i.$x} = ${'champ_axe_calcule'.$i.$x};
								if($x === 1)
								{
									${'chaine_calculee'.$i.$x} = ' <strong>'.str_replace('['.${'champ_axe_calcule'.$i.$x}.']','<a class="noaxis" Title="Cherche un autre axe calculé/montant conditonnel du nom de \''.${'nom_champ_axe'.$i.$x}.'\'" rel="tooltip">['.${'nom_champ_axe'.$i.$x}.']</a>',${'chaine_calculee_pre'.$i.$x}).'</strong></em>';
								}
								else
								{
									${'chaine_calculee'.$i.$x} = ' <strong>'.str_replace('['.${'champ_axe_calcule'.$i.$x}.']','<a class="noaxis" Title="Cherche un autre axe calculé/montant conditonnel du nom de \''.${'nom_champ_axe'.$i.$x}.'\'" rel="tooltip">['.${'nom_champ_axe'.$i.$x}.']</a>',${'nom_formulax'.$i.($x-1)}).'</strong></em>';
								}
							}	
							
							
							${'nom_formulax'.$i.$x}  = ${'chaine_calculee'.$i.$x} ; // On ramène la formule calculé, transformée ou non, vers la variable ramenant la formule dans la page web
							${'nom_formulax'.$i}  = ${'nom_formulax'.$i.$x} ;
							$x++;
						}	// FIN DE LA BOUCLE

					}
					
					elseif ((${'nature_axe_montant'.$i} == '0')) // Si montant conditionnel
					{${'nature_axe_montant'.$i} = '<strong>Montant conditionnel</strong>'; 
					// Recherche la formule du montant conditionnel
					${'debut_axe_formulax'.$i}   = strpos(${'chaine_champ_ali'.$i},'F=');
					${'fin_axe_formulax'.$i} = strpos(${'chaine_champ_ali'.$i},'Y=');
					${'valeur_axe_formulax'.$i} = substr(${'chaine_champ_ali'.$i},${'debut_axe_formulax'.$i}  +2,(${'fin_axe_formulax'.$i}-3)-(${'debut_axe_formulax'.$i}  )); 
					
					// TEST RECUP CHAMPS Montant Conditionnel ====================================
						
						${'valeur_montantcond_formulax'.$i} = ${'valeur_axe_formulax'.$i} ;
						$nb_de_champs_calcules = substr_count(${'valeur_montantcond_formulax'.$i},'['); // On compte le nombre de champs utilisés dans le montant cond.
						if($nb_de_champs_calcules === 0){${'nom_formulax'.$i} = '<strong>'.${'valeur_axe_formulax'.$i}.'</strong>';} // S'il n'y en a pas, on renvoi la formule telle quelle
						$y=1;
						while ($y <= $nb_de_champs_calcules) // Boucle pour segmenter les champs du montant conditionnel
						{
							if($y === 1)
							{	
								${'chaine_montant_calculee_pre'.$i.$y} = ${'valeur_montantcond_formulax'.$i}; // Si on est au premier champ, on prend la formule en entier
							}
							else
							{
								${'chaine_montant_calculee_pre'.$i.$y} = substr(${'chaine_montant_calculee_pre'.$i.($y-1)},(${'ref_montantcond_fin'.$i.($y-1)}+1),9999999); // Sinon on prend la formule transformé -1 et on répète le process précédent depuis cette chaine
							}
							
							${'ref_montantcond_debut'.$i.$y} = strpos(${'chaine_montant_calculee_pre'.$i.$y},'['); // Trouver le prochain champ exactement (debut, puis fin).
							${'ref_montantcond_fin'.$i.$y} = strpos(${'chaine_montant_calculee_pre'.$i.$y},']');
							${'champ_montantcond_calcule'.$i.$y} =  substr(${'chaine_montant_calculee_pre'.$i.$y},${'ref_montantcond_debut'.$i.$y},(${'ref_montantcond_fin'.$i.$y}-${'ref_montantcond_debut'.$i.$y}+1)); // On isole le [Champ]
							${'champ_montantcond_calcule'.$i.$y} = substr(${'champ_montantcond_calcule'.$i.$y},1,-1); // On ramène l'intérieur du [Champ] (soit "Champ")
								
							if(is_numeric(${'champ_montantcond_calcule'.$i.$y}) OR substr_count(${'champ_montantcond_calcule'.$i.$y},'|') === 1) // Si la valeur du [Champ] est numérique ou virtualisé, on le cherche en BDD et on boucle pour chaque champ détecté
							{
										if(is_numeric(${'champ_montantcond_calcule'.$i.$y}))
										{	
											// Requête pour récupérer le nom du champ
											${'requete_nom_champ_montantcond'.$i.$y} = $bdd->query("
															SELECT TOP 1
															ID_MC as id_mc
															,DESIGNATION as desig
															,SOURCE as source
															FROM
															DL_MODELES_CHAMPS
															WHERE
															ID_VUE = '".$nom_modele."' AND ID_DL = '".$nom_connecteur."' AND ID_MC = '".${'champ_montantcond_calcule'.$i.$y}."'
															");
											${'fetch_nom_champ_montantcond'.$i.$y} = ${'requete_nom_champ_montantcond'.$i.$y}->fetch();
											${'id_champ_montantcond'.$i.$y} = ${'fetch_nom_champ_montantcond'.$i.$y}['id_mc'];
											${'nom_champ_montantcond'.$i.$y} = ${'fetch_nom_champ_montantcond'.$i.$y}['desig'];
											${'source_champ_montantcond'.$i.$y} = ${'fetch_nom_champ_montantcond'.$i.$y}['source'];
											
											if(strlen(${'nom_champ_montantcond'.$i.$y}) === 0) // Si la requête ID_MC du champ n'a ramené aucun résultat, on renvoi un champ rouge
											{
												${'id_champ_montantcond'.$i.$y} = ${'champ_montantcond_calcule'.$i.$y};
												${'nom_champ_montantcond'.$i.$y} = '<a class="red" title="Champ introuvable ! L\'ID affiché n\'existe pas dans le dictionnaire." rel="tooltip">['.${'id_champ_montantcond'.$i.$y}.']</a>';
												if($y === 1)
												{
													${'chaine_montant_calculee'.$i.$y} = ' <strong>'.str_replace('['.${'champ_montantcond_calcule'.$i.$y}.']',${'nom_champ_montantcond'.$i.$y},${'chaine_montant_calculee_pre'.$i.$y}).'</strong></em>';
												}
												else
												{
													${'chaine_montant_calculee'.$i.$y} = ' <strong>'.str_replace('['.${'champ_montantcond_calcule'.$i.$y}.']',${'nom_champ_montantcond'.$i.$y},${'nom_formulax'.$i.($y-1)}).'</strong></em>';
												}
											}
											
											else // Sinon on l'affiche dans la chaine de la formule du montant cond.
											{	
												if($y === 1)
												{
													${'chaine_montant_calculee'.$i.$y} = ' <strong>'.str_replace('['.${'champ_montantcond_calcule'.$i.$y}.']','<a title="<strong>ID du champ :</strong> '.${'id_champ_montantcond'.$i.$y}.'<br><strong>Source :</strong> '.str_replace('"','',${'source_champ_montantcond'.$i.$y}).' " rel="tooltip">['.${'nom_champ_montantcond'.$i.$y}.']</a>',${'chaine_montant_calculee_pre'.$i.$y}).'</strong></em>';
												}
												else
												{
													${'chaine_montant_calculee'.$i.$y} = ' <strong>'.str_replace('['.${'champ_montantcond_calcule'.$i.$y}.']','<a title="<strong>ID du champ :</strong> '.${'id_champ_montantcond'.$i.$y}.'<br><strong>Source :</strong> '.str_replace('"','',${'source_champ_montantcond'.$i.$y}).' " rel="tooltip">['.${'nom_champ_montantcond'.$i.$y}.']</a>',${'nom_formulax'.$i.($y-1)}).'</strong></em>';
												}
												}
										}	
										
										elseif(substr_count(${'champ_montantcond_calcule'.$i.$y},'|') === 1) // Si le champ du montant cond est un dossier virtuel
										{
											${'id_champ_mc_virtuel_montantcond'.$i.$y} = strpos(${'champ_montantcond_calcule'.$i.$y}, '|'); // Pour dossier virtuel
											${'id_champ_mc_virtuel_dossier_montantcond'.$i.$y} = substr(${'champ_montantcond_calcule'.$i.$y},0,${'id_champ_mc_virtuel_montantcond'.$i.$y}); // Récup de l'ID du dossier virtuel
											${'id_champ_mc_virtuel_champ_montantcond'.$i.$y} = substr(${'champ_montantcond_calcule'.$i.$y},${'id_champ_mc_virtuel_montantcond'.$i.$y}+1,strlen(${'champ_montantcond_calcule'.$i.$y})); // Récup de l'ID MC du dossier virtuel
											//Requête pour récupérer le nom du champ virtuel
											${'requete_nom_champ_virtuelle'.$i.$y} = $bdd->query("
													SELECT 
													  DL_MODELES_CHAMPS.ID_MC as id_mc
													  ,DL_MODELES_CHAMPS.SOURCE as source
													  ,DL_MODELES_CHAMPS.DESIGNATION as desig
													  ,DL_VUES.DESIGNATION as desig_vue
													  FROM DL_MODELES_CHAMPS
													  LEFT JOIN DL_VUES
													  ON
													  DL_VUES.ID_DL = DL_MODELES_CHAMPS.ID_DL 
													  AND
													  DL_VUES.ID_VUE = DL_MODELES_CHAMPS.ID_VUE
													  LEFT JOIN DL_VUES_VIRTUELLES
													  ON
													  DL_MODELES_CHAMPS.ID_DL = DL_VUES_VIRTUELLES.ID_DL
													  AND
													  DL_MODELES_CHAMPS.ID_VUE = DL_VUES_VIRTUELLES.ID_VUE_VIRTUELLE
													  WHERE 
													  DL_MODELES_CHAMPS.ID_MC = '".${'id_champ_mc_virtuel_champ_montantcond'.$i.$y}."'
													  AND
													  DL_MODELES_CHAMPS.ID_DL = '".$nom_connecteur."'
													  AND
													  DL_VUES_VIRTUELLES.ID = '".${'id_champ_mc_virtuel_dossier_montantcond'.$i.$y}."'
														");
											${'fetch_nom_champ_virtuelle_montantcond'.$i.$y} = ${'requete_nom_champ_virtuelle'.$i.$y}->fetch();
											${'id_champ_montantcond'.$i.$y} = ${'fetch_nom_champ_virtuelle_montantcond'.$i.$y}['id_mc'];
											${'nom_champ_montantcond'.$i.$y} = ${'fetch_nom_champ_virtuelle_montantcond'.$i.$y}['desig'];
											${'source_champ_montantcond'.$i.$y} = ${'fetch_nom_champ_virtuelle_montantcond'.$i.$y}['source'];
											${'nom_vue_virtuelle_montantcond'.$i.$y} = ${'fetch_nom_champ_virtuelle_montantcond'.$i.$y}['desig_vue'];
											
											
											
											if(strlen(${'nom_champ_montantcond'.$i.$y}) === 0) // Si la requête du champ virtuel n'a ramené aucun résultat, on renvoi le champ en rouge avec l'ID
											{
												${'id_champ_montantcond'.$i.$y} = ${'champ_montantcond_calcule'.$i.$y};
												${'nom_champ_montantcond'.$i.$y} = '<a class="red" title="Champ VIRTUEL introuvable ! L\'ID affiché n\'existe pas dans le dictionnaire." rel="tooltip">['.${'id_champ_montantcond'.$i.$y}.']</a>';
												if($y === 1)
												{
													${'chaine_montant_calculee'.$i.$y} = ' <strong>'.str_replace('['.${'champ_montantcond_calcule'.$i.$y}.']',${'nom_champ_montantcond'.$i.$y},${'chaine_montant_calculee_pre'.$i.$y}).'</strong></em>';
												}
												else
												{
													${'chaine_montant_calculee'.$i.$y} = ' <strong>'.str_replace('['.${'champ_montantcond_calcule'.$i.$y}.']',${'nom_champ_montantcond'.$i.$y},${'nom_formulax'.$i.($y-1)}).'</strong></em>';
												}	
											}
											
											else // Sinon on remplace l'ID virtuel du montant cond. par la désignation remontée
											{
												if($y === 1)
												{
													${'chaine_montant_calculee'.$i.$y} = ' <strong>'.str_replace('['.${'champ_montantcond_calcule'.$i.$y}.']','<a class="virtual" title="<strong>ID du champ :</strong> '.${'id_champ_montantcond'.$i.$y}.'<br><strong>Source (VIRTUELLE) :</strong> '.str_replace('"','',${'source_champ_montantcond'.$i.$y}).' " rel="tooltip">'.${'nom_champ_montantcond'.$i.$y}.'</a>',${'chaine_montant_calculee_pre'.$i.$y}).'</strong></em>';	
												}
												else
												{
													${'chaine_montant_calculee'.$i.$y} = ' <strong>'.str_replace('['.${'champ_montantcond_calcule'.$i.$y}.']','<a class="virtual" title="<strong>ID du champ :</strong> '.${'id_champ_montantcond'.$i.$y}.'<br><strong>Source (VIRTUELLE) :</strong> '.str_replace('"','',${'source_champ_montantcond'.$i.$y}).' " rel="tooltip">'.${'nom_champ_montantcond'.$i.$y}.'</a>',${'nom_formulax'.$i.($y-1)}).'</strong></em>';
												}
											}	
										}	
											
										else										
										{
											${'champ_montantcond_calcule'.$i.$y} = ${'id_champ_montantcond'.$i.$y};
											${'nom_champ_montantcond'.$i.$y} = 'Champ axe '.$i.$y.' inconnu (pas sur la base - ID : '.${'id_champ_montantcond'.$i.$y}.')';
										}
							}
							
							else // Si le champ de la boucle n'est pas un ID, on suppose une référence à un autre champ calculé/montant cond. (champ marron)
							{
								
								${'nom_champ_montantcond'.$i.$y} = ${'champ_montantcond_calcule'.$i.$y};
								if($y === 1)
								{
									${'chaine_montant_calculee'.$i.$y} = ' <strong>'.str_replace('['.${'champ_montantcond_calcule'.$i.$y}.']','<a class="noaxis" Title="Cherche un autre Montant Conditionnel/montant conditonnel du nom de \''.${'nom_champ_montantcond'.$i.$y}.'\'" rel="tooltip">'.${'nom_champ_montantcond'.$i.$y}.'</a>',${'chaine_montant_calculee_pre'.$i.$y}).'</strong></em>';
								}
								else
								{
									${'chaine_montant_calculee'.$i.$y} = ' <strong>'.str_replace('['.${'champ_montantcond_calcule'.$i.$y}.']','<a class="noaxis" Title="Cherche un autre Montant Conditionnel/montant conditonnel du nom de \''.${'nom_champ_montantcond'.$i.$y}.'\'" rel="tooltip">'.${'nom_champ_montantcond'.$i.$y}.'</a>',${'nom_formulax'.$i.($y-1)}).'</strong></em>';
								}
							}	
							
							
							${'nom_formulax'.$i.$y}  = ${'chaine_montant_calculee'.$i.$y} ; // On ramène la formule calculé, transformée ou non, vers la variable ramenant la formule dans la page web
							${'nom_formulax'.$i}  = ${'nom_formulax'.$i.$y};
							$y++;
						}	

					}
					
					else {${'nature_axe_montant'.$i} = 'Axe calculé / Montant conditionnel'; ${'valeur_axe_formulax'.$i} = ''; ${'nom_formulax'.$i} = '';}

					${'nom_detail_champ'.$i} = 'Champ '.$i.' : 
					'.${'nature_axe_montant'.$i}.' du nom de "<strong>'.${'valeur_axe_montant'.$i}.'</strong>" '.${'nom_agreg_champ'.$i}.' - <em>
					                             <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												 Formule : '.${'nom_formulax'.$i}.'</em><br/>';
					${'nom_du_mod_champs'.$i} =  '';
					${'nom_source_mod_champs'.$i} = '';
	}	

	 else
	{
	${'longueur_difference'.$i} = (strpos(${'chaine_champ_ali'.$i},'G')-1)-(strpos(${'chaine_champ_ali'.$i},'S')+2); // Calcul de la longueur de l'ID
	
	${'id_champ_mc_base'.$i} = substr(${'chaine_champ_ali'.$i},strpos(${'chaine_champ_ali'.$i},'S')+2,strlen(substr(${'chaine_champ_ali'.$i},strpos(${'chaine_champ_ali'.$i},'S')+2,${'longueur_difference'.$i}))); // Récup de l'ID du champ
	${'id_champ_mc'.$i} = ${'id_champ_mc_base'.$i};

	if(is_numeric(${'id_champ_mc'.$i})) // Débogage
		{

			// Requête pour récupérer le nom du champ
			${'requete_nom_champ'.$i} = $bdd->query("
							SELECT TOP 1
							ID_MC as id_mc
							,DESIGNATION as desig
							,SOURCE as source
							FROM
							DL_MODELES_CHAMPS
							WHERE
							ID_VUE = '".$nom_modele."' AND ID_DL = '".$nom_connecteur."' AND ID_MC = '".${'id_champ_mc'.$i}."'
							");
			${'fetch_nom_champ'.$i} = ${'requete_nom_champ'.$i}->fetch();
			${'id_champ'.$i} = ${'fetch_nom_champ'.$i}['id_mc'];
			${'nom_champ'.$i} = ${'fetch_nom_champ'.$i}['desig'];
			${'source_champ'.$i} = ${'fetch_nom_champ'.$i}['source'];
			
			if(strlen(${'nom_champ'.$i}) === 0)
			{
				${'nom_detail_champ'.$i} = 'Champ '.$i.' inconnu (pas sur la base - ID : '.${'id_champ_mc'.$i}.') '.${'nom_agreg_champ'.$i}.'<br>';
			}

			else
			{	
				${'nom_detail_champ'.$i} = 'Champ '.$i.' : <a title="<strong>ID du champ :</strong> '.${'id_champ'.$i}.'<br><strong>Source :</strong> '.str_replace('"','',${'source_champ'.$i}).' " rel="tooltip"><strong>'.${'nom_champ'.$i}.'</strong></a> '.${'nom_agreg_champ'.$i}.'<br>';
			}	
		}
		
	else // Si pas de correspondance en base on note un message d'erreur ou on vérifie si c'est un dossier virtuel
		{	
			${'id_champ_mc_virtuel'.$i} = strpos(${'id_champ_mc'.$i}, '|'); // Pour dossier virtuel
			if(${'id_champ_mc_virtuel'.$i} <> NULL) // Si le champ est bien un dossier virtuel
			{
				${'id_champ_mc_virtuel_dossier'.$i} = substr(${'id_champ_mc'.$i},0,${'id_champ_mc_virtuel'.$i}); // Récup de l'ID du dossier virtuel
				${'id_champ_mc_virtuel_champ'.$i} = substr(${'id_champ_mc'.$i},${'id_champ_mc_virtuel'.$i}+1,strlen(${'id_champ_mc'.$i})); // Récup de l'ID MC du dossier virtuel
				// Requête pour récupérer le nom du champ virtuel
				${'requete_nom_champ_virtuelle'.$i} = $bdd->query("
						SELECT 
						  DL_MODELES_CHAMPS.ID_MC as id_mc
						  ,DL_MODELES_CHAMPS.SOURCE as source
						  ,DL_MODELES_CHAMPS.DESIGNATION as desig
						  ,DL_VUES.DESIGNATION as desig_vue
						  FROM DL_MODELES_CHAMPS
						  LEFT JOIN DL_VUES
						  ON
						  DL_VUES.ID_DL = DL_MODELES_CHAMPS.ID_DL 
						  AND
						  DL_VUES.ID_VUE = DL_MODELES_CHAMPS.ID_VUE
						  LEFT JOIN DL_VUES_VIRTUELLES
						  ON
						  DL_MODELES_CHAMPS.ID_DL = DL_VUES_VIRTUELLES.ID_DL
						  AND
						  DL_MODELES_CHAMPS.ID_VUE = DL_VUES_VIRTUELLES.ID_VUE_VIRTUELLE
						  WHERE 
						  DL_MODELES_CHAMPS.ID_MC = '".${'id_champ_mc_virtuel_champ'.$i}."'
						  AND
						  DL_MODELES_CHAMPS.ID_DL = '".$nom_connecteur."'
						  AND
						  DL_VUES_VIRTUELLES.ID = '".${'id_champ_mc_virtuel_dossier'.$i}."'
							");
				${'fetch_nom_champ_virtuelle'.$i} = ${'requete_nom_champ_virtuelle'.$i}->fetch();
				${'id_champ'.$i} = ${'fetch_nom_champ_virtuelle'.$i}['id_mc'];
				${'nom_champ'.$i} = ${'fetch_nom_champ_virtuelle'.$i}['desig'];
				${'source_champ'.$i} = ${'fetch_nom_champ_virtuelle'.$i}['source'];
				${'nom_vue_virtuelle'.$i} = ${'fetch_nom_champ_virtuelle'.$i}['desig_vue'];
				
				
				if(strlen(${'nom_champ'.$i}) === 0)
				{
					${'nom_detail_champ'.$i} = 'Champ '.$i.' inconnu (virtualisé - pas sur la base - ID : '.${'id_champ_mc'.$i}.') '.${'nom_agreg_champ'.$i}.'<br>';
				}

				else
				{	
					${'nom_detail_champ'.$i} = 'Champ '.$i.' : <a title="<strong>ID du champ :</strong> '.${'id_champ'.$i}.'<br><strong>Source :</strong> '.str_replace('"','',${'source_champ'.$i}).' " rel="tooltip"><strong>'.${'nom_champ'.$i}.'</strong> <em>(Champ virtualisé - Modèle d\'origine : '.${'nom_vue_virtuelle'.$i}.')</em></a>'.${'nom_agreg_champ'.$i}.'<br/>';
				}
			}
				
			else // Si ce n'est pas un champ normal ou virtuel, on inscrit le message d'erreur
			{
				${'nom_detail_champ'.$i} = 'Champ '.$i.' introuvable :<em> Rapport =  ID MC is '.${'id_champ_mc'.$i}.'</em><br/>';
			}	
		}


    }
	$i++;
}

$nb_carac_filtres_debut = strpos($formule_sans_reliures,'@R'); // Check de présence filtres

// Mise en place pour filtres avec reF. cellule (s'il y a)
$chaine_filtres_mep = strstr($formule_sans_reliures,'@R'); // Précaution pour commencer la boucle des ref. cellule à la fin de la formule
$chaine_ref_cellule_mep = str_replace(' ','',strstr($chaine_filtres_mep,'";')); // Mise en place de la chaine de filtres, avec précautions s'il y a des espaces dans le copier-coller
$fin_chaine_ref_cellule = strpos($chaine_ref_cellule_mep, ')');
$chaine_ref_cellule_brut = substr($chaine_ref_cellule_mep,strlen($chaine_ref_cellule_mep)-$fin_chaine_ref_cellule);
$chaine_ref_cellule = str_replace(')','',str_replace('"','',$chaine_ref_cellule_brut));
if(strlen($chaine_ref_cellule) > 0) // S'il y a bien des ref aux cellules dans les filtres
{
	$nb_de_ref_cellule = substr_count($chaine_ref_cellule,';');
	$q =1;
	while ($q <= $nb_de_ref_cellule) // Boucle pour segmenter les ref. cellules
	{
		if($q === 1) // Si on est sur la première ref, on définit la première chaine
		{
		${'cellule_ref_brut'.$q} = $chaine_ref_cellule;
		}	
		else
		{
			${'cellule_ref_brut'.$q} = substr(${'cellule_ref_sans_fin'.($q-1)},strpos(${'cellule_ref_sans_fin'.($q-1)},';'));
		}
	
		${'cellule_ref_sans_fin'.$q} = substr(${'cellule_ref_brut'.$q},1);
		${'cellule_ref_fin'.$q} = strpos(${'cellule_ref_sans_fin'.$q},';');
		if(${'cellule_ref_fin'.$q} <> NULL)
		{	
			${'cellule_ref'.$q} = substr(${'cellule_ref_sans_fin'.$q},0,${'cellule_ref_fin'.$q});
		}
		else
		{
			${'cellule_ref'.$q} = ${'cellule_ref_sans_fin'.$q};
		}
		
		if(strlen(${'cellule_ref'.$q}) >= 4 AND substr_count(${'cellule_ref'.$q},'$') === 2) // Si la ref. cellule est invariante
		{
			${'cellule_ref_filtres'.$q} = 'Cellule '.str_replace('$','',${'cellule_ref'.$q}). ' (Invariant)';
		}	
		elseif(substr(${'cellule_ref'.$q},0,1) === '$' AND substr_count(${'cellule_ref'.$q},'$') === 1) // Si la ref. cellule est variante en ligne uniquement
		{	
			${'cellule_ref_filtres'.$q} = 'Cellule '.str_replace('$','',${'cellule_ref'.$q}). ' (Variant en ligne)';
		}
		elseif(substr_count(${'cellule_ref'.$q},'$') === 0) // Si la ref. cellule est variante en colonnes uniquement
		{
			${'cellule_ref_filtres'.$q} = 'Cellule '.str_replace('$','',${'cellule_ref'.$q}). ' (Variant en ligne et en colonne)';
		}	
		else
		{
			${'cellule_ref_filtres'.$q} = 'Cellule '.str_replace('$','',${'cellule_ref'.$q}). ' (Variant en colonne)';
		}	
		
		$q++;
	}	
}


			
	if($nb_carac_filtres_debut <> NULL) // Si on a des filtres
	{
		$debut_axe_filtres = strpos($formule_sans_reliures,'@R');
		$presence_filtres_avancees = strpos(strstr($formule_sans_reliures,'@R'), ':@'); // Check présence filtre avancés
				
		if ($presence_filtres_avancees <> NULL) // Si on a des filtres avancés ou non, on adapte la fin axe filtres
		{
			$fin_axe_filtres = strpos($formule_sans_reliures,':@')+1; // Fin de la formule
		}
		else
		{
			$fin_axe_filtres = strpos($formule_sans_reliures,':"')+1; // Fin de la formule
		}	
				
		$valeur_axe_filtres = substr($formule_sans_reliures,$debut_axe_filtres+1,(($fin_axe_filtres)-1)-($debut_axe_filtres)); // FAUX MAIS FONCTIONNE - NE DEVRAIT PAS NON PLUS ETRE UTILISÉ POUR LES FILTRES AVANCÉS (plus bas), à refaire si le temps
		$nb_de_filtres = mb_substr_count($valeur_axe_filtres, 'R='); // Compte le nombre de filtres
		
			$p = 1;
			while ($p <= ($nb_de_filtres)) // Boucle pour segmenter les autres chaines de filtres (s'il y a)
			{
				if($p === 1) // Si on est au premier filtre, le traitement est différent
				{
					// Ordre du filtres
					
					${'chaine_filtre_global'.$p} = strstr($formule_sans_reliures,'R=');
					${'debut_filtre'.$p} = strpos(${'chaine_filtre_global'.$p},'R');
					${'fin_filtre'.$p} = strpos(${'chaine_filtre_global'.$p},':');
					${'chaine_filtre'.$p} = substr(${'chaine_filtre_global'.$p}, ${'debut_filtre'.$p}, ${'fin_filtre'.$p});
							
					// Recup des arguments du premier filtre : 
								
					${'chaine_champ_filtre'.$p} = substr(${'chaine_filtre'.$p},strpos(${'chaine_filtre'.$p},'S')+2,(strpos(${'chaine_filtre'.$p},'V'))-7);
				}	
						
				else
				{
					// Définition chaine de chaque filtre
					${'chaine_filtre_global'.$p} = substr(strstr(${'chaine_filtre_global'.($p-1)},':'),1,strlen(strstr(${'chaine_filtre_global'.($p-1)},':')));
					${'debut_filtre'.$p} = 0;
					${'fin_filtre'.$p} = strpos(${'chaine_filtre_global'.$p},':');
					${'chaine_filtre'.$p} = substr(${'chaine_filtre_global'.$p},${'debut_filtre'.$p},${'fin_filtre'.$p});
				}	
						
				// Définition des arguments des filtres
						
				${'chaine_champ_filtre'.$p} = substr(${'chaine_filtre'.$p},strpos(${'chaine_filtre'.$p},'S')+2,(strpos(${'chaine_filtre'.$p},'V'))-7);
						
				if(is_numeric(${'chaine_champ_filtre'.$p} )) // Débogage
				{
						
				// Requête pour récupérer le champ du filtre
				${'nom_reel_filtre_champ'.$p} = $bdd->query("
				SELECT TOP 1
				ID_MC as id_mc
				,DESIGNATION as desig
				,SOURCE as source
				FROM
				DL_MODELES_CHAMPS
				WHERE
				ID_VUE = '".$nom_modele."' AND ID_DL = '".$nom_connecteur."' AND ID_MC = '".${'chaine_champ_filtre'.$p}."'
				");
						
				${'nom_filtre_champ_reel'.$p} = ${'nom_reel_filtre_champ'.$p}->fetch();
				${'id_filtre_champ'.$p} = ${'nom_filtre_champ_reel'.$p}['id_mc'];
				${'nom_filtre_champ'.$p} = ${'nom_filtre_champ_reel'.$p}['desig'];
				${'source_filtre_champ'.$p} = ${'nom_filtre_champ_reel'.$p}['source'];
				
				if(strlen(${'nom_filtre_champ'.$p}) === 0)
				{
					${'nom_filtre_champ'.$p} = 'Filtre '.$p.' inconnu (pas sur la base - <em>ID remonté : '.${'chaine_champ_filtre'.$p}.'</em>)';
				}		
				// Récup de la valeur du filtre
						
				${'recup_valeur_filtre'.$p} = strstr(${'chaine_filtre'.$p},'V=');
				${'valeur_filtre'.$p} = substr(${'recup_valeur_filtre'.$p},2);
						
				}
						
				else // SI l'ID du champ filtre n'est pas numérique, on vérifie si ce n'est pas un champ virtualisé, sinon on créé un ligne de débogage
				{
					${'chaine_champ_filtre_virtuel'.$p} = strpos(${'chaine_champ_filtre'.$p}, '|'); // Pour dossier virtuel
					if(${'chaine_champ_filtre_virtuel'.$p} <> NULL) // Si le champ est bien un dossier virtuel
					{
						${'chaine_champ_filtre_virtuel_dossier'.$p} = substr(${'chaine_champ_filtre'.$p},0,${'chaine_champ_filtre_virtuel'.$p}); // Récup de l'ID du dossier virtuel
						${'chaine_champ_filtre_champ'.$p} = substr(${'chaine_champ_filtre'.$p},${'chaine_champ_filtre_virtuel'.$p}+1,strlen(${'chaine_champ_filtre'.$p})); // Récup de l'ID MC du dossier virtuel
						// Requête pour récupérer le nom du champ virtuel
						${'requete_champ_filtre_virtuelle'.$p} = $bdd->query("
								SELECT 
								  DL_MODELES_CHAMPS.ID_MC as id_mc
								  ,DL_MODELES_CHAMPS.SOURCE as source
								  ,DL_MODELES_CHAMPS.DESIGNATION as desig
								  ,DL_VUES.DESIGNATION as desig_vue
								  FROM DL_MODELES_CHAMPS
								  LEFT JOIN DL_VUES
								  ON
								  DL_VUES.ID_DL = DL_MODELES_CHAMPS.ID_DL 
								  AND
								  DL_VUES.ID_VUE = DL_MODELES_CHAMPS.ID_VUE
								  LEFT JOIN DL_VUES_VIRTUELLES
								  ON
								  DL_MODELES_CHAMPS.ID_DL = DL_VUES_VIRTUELLES.ID_DL
								  AND
								  DL_MODELES_CHAMPS.ID_VUE = DL_VUES_VIRTUELLES.ID_VUE_VIRTUELLE
								  WHERE 
								  DL_MODELES_CHAMPS.ID_MC = '".${'chaine_champ_filtre_champ'.$p}."'
								  AND
								  DL_MODELES_CHAMPS.ID_DL = '".$nom_connecteur."'
								  AND
								  DL_VUES_VIRTUELLES.ID = '".${'chaine_champ_filtre_virtuel_dossier'.$p}."'
									");
						${'fetch_champ_filtre_virtuelle'.$p} = ${'requete_champ_filtre_virtuelle'.$p}->fetch();
						${'id_filtre_champ'.$p} = ${'fetch_champ_filtre_virtuelle'.$p}['id_mc'];
						${'nom_filtre_champ_remontee'.$p} = ${'fetch_champ_filtre_virtuelle'.$p}['desig'];
						${'source_filtre_champ'.$p} = ${'fetch_champ_filtre_virtuelle'.$p}['source'];
						${'nom_vue_virtuelle'.$p} = ${'fetch_champ_filtre_virtuelle'.$p}['desig_vue'];
						${'recup_valeur_filtre'.$p} = strstr(${'chaine_filtre'.$p},'V=');
						${'valeur_filtre'.$p} = substr(${'recup_valeur_filtre'.$p},2);
						
						if(strlen(${'chaine_champ_filtre_champ'.$p}) === 0 XOR strlen(${'nom_filtre_champ_remontee'.$p}) === 0)
						{
							${'nom_filtre_champ'.$p} = 'Filtre '.$p.' inconnu (virtualisé - pas sur la base -<em> ID remonté : '.${'chaine_champ_filtre'.$p}.'</em>)';
						}

						else
						{	
							${'nom_filtre_champ'.$p} = ${'nom_filtre_champ_remontee'.$p}. '<em></strong></a> (Champ virtualisé - Modèle d\'origine : '.${'nom_vue_virtuelle'.$p}.')</em>';
						}
					}
					else // Si ce n'est pas un dossier virtuel
					{
							${'nom_filtre_champ'.$p} = 'Filtre '.$p.' inconnu (ID retrouvé : '.${'chaine_champ_filtre_champ'.$p}.') - Valeur : "'.${'valeur_filtre'.$p}.'"<br>';
					}		
				}		
				
				// Mise en place pour retrouver les références aux cellules que font les filtres (si référence il y a)
				${'accolade_filtre_debut'.$p} = strpos(${'valeur_filtre'.$p},'{');
				${'accolade_filtre_fin'.$p} = strpos(${'valeur_filtre'.$p},'}');
				
				if(is_numeric(${'accolade_filtre_debut'.$p}) AND is_numeric(${'accolade_filtre_fin'.$p})) // Si on a bien une ref. à un cellule du classeur
					{
						$s = (str_replace('}','',(str_replace('{','',${'valeur_filtre'.$p})))+1); // Trouver le numéro ref de la cellule
						${'valeur_filtre'.$p} = ${'cellule_ref_filtres'.$s}; // On fait référence à la ref défini plus haut
					}	
				
				// Définition finale du filtre
						
				if($p === 1) // Si on est au premier filtre, le traitement est différent  (on ajout l'en-tête du paragraphe filtre)
				{
					${'nom_du_filtre'.$p} = '<br><span class="titre_params_rubriques">Détails filtre(s) :</span> <hr>
					Filtre sur : <a title="<strong>ID du champ :</strong> '.${'id_filtre_champ'.$p}.'<br><strong>Source :</strong> '.str_replace('"','',${'source_filtre_champ'.$p}).' " rel="tooltip"><strong>'.${'nom_filtre_champ'.$p}.'</strong></a> - Valeur : "'.${'valeur_filtre'.$p}.'"<br/><br/>';	
				}
				else
				{
					${'nom_du_filtre'.$p} = 'Filtre sur : <a title="<strong>ID du champ :</strong> '.${'id_filtre_champ'.$p}.'<br><strong>Source :</strong> '.${'source_filtre_champ'.$p}.' " rel="tooltip"><strong>'.${'nom_filtre_champ'.$p}.'</strong></a> - Valeur : "'.${'valeur_filtre'.$p}.'"<br/><br/>';
				}	
				$p++;

			}
				
		if ($presence_filtres_avancees <> NULL) // Si on a des filtres avancés ou non - A CORRIGER SI REF. CELLULE
		{
			$chaine_filtres_avances_base = strstr(str_replace('"','',$valeur_axe_filtres), ':@');
			$chaine_filtres_avances = substr($chaine_filtres_avances_base,2);
			$filtres_avances = 'Filtres avancés : "'.$chaine_filtres_avances. '"<br/<br/>';
		}	
				
		else
		{
			$filtres_avances = '';
		}
				
	}
			
	else
	{
			$nb_de_filtres = 1;
			$nom_du_filtre1 = '';
			$filtres_avances = '';
	}	

$nom_axe_montant = '';
$nom_du_mod_champs =  '';
$nom_source_mod_champs = '';
$nom_axe_agreg = '';
$nom_formulax = '';
$nom_axe_filtres = '';
$nb_de_champs_acu = 0;
$nb_de_caches = '';
$nb_de_caches_numeric = 0;
	

?>