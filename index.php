<!DOCTYPE html>

<html>

    <head>

        <meta charset="utf-8" />
        <title>Insider - Connexion à la MasterReporting</title>
		<meta name="description" content="Insider" />
		<link rel="icon" type="image/png" href="../images/favicon.png" />
		<link rel="stylesheet" href="inside.css" />
        <!--[if IE]><link rel="shortcut icon" type="image/x-icon" href="favicon.ico" /><![endif]-->		
</head>
	<body class="back_parametres">
	<a href="index.php"><img src="images/insider.PNG"/>Version 1.4</a> 
	<br/><br/>
	<form  action="master.php?avec_bdd=yes" class="rubriques_params" method="post">
	<span class="titre_params">Connexion à la MasterReporting :</span> <br/><br/>
	<label id="formule" name="formule">Serveur\DSN (local): &nbsp;</label>
	<input type="text" placeholder="Serveur\DSN" name="serveur" size="60" id="serveur" required>
	<br/><br/>
	<label id="formule" name="formule">Nom de la BDD :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
	<input type="text" placeholder="Nom de la BDD" name="bdd" size="60" id="bdd" required>
	<br/><br/>
	<label id="formule" name="formule">Utilisateur : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
	<input type="text" placeholder="Utilisateur" name="user" size="60" id="user" required>
	<br/><br/>
	<label id="formule" name="formule">Mot de passe :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </label>
	<input type="password" placeholder="Mot de passe" name="mdp" size="60" id="mdp" required>
	<br/><br/><br/>
	<input type="submit" name="informations" value="Informations sur la base" class="validate"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<input type="submit" name="interprete" value="Interprétateur de formules" class="validate"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<input type="submit" name="xml" value="Télécharger anciennes versions du dictionnaire" class="validate"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	</form>
	<br/><br/><br/>
	<hr class="barre">
	<br/><br/><br/>
	<form  action="master.php?avec_bdd=no" class="rubriques_params" method="post">
	<span class="titre_params">Actions sans connexion à une MasterReporting (désactivé, Work in Progress) :</span> <br/><br/><br/>
	<input type="submit" name="interprete" value="Interprétateur de formules" class="validate" disabled="disabled"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	</form>
	
	</body>
</html>