<?php
	include("header.php"); // Inclusion de la banierre 
	$ip = gethostbyaddr($_SERVER['REMOTE_ADDR']);
	if(file_exists("connexion/info_connexion".$ip.".php") AND file_exists("connexion/info_connexion_chaine".$ip.".php"))
	{	
		include("connexion/info_connexion".$ip.".php"); // Récup de la chaine de donnees^^
		include("connexion/info_connexion_chaine".$ip.".php"); // Récup de la chaine de donnees
	}
	else
	{
		echo '<br><span class="red">Impossible de trouver votre source de données ! Veuillez cliquez sur le logo "Insider" et resaisir votre source.</span><br><br>';
	}
	if(isset($bdd))
	{
		include("details_connexions.php"); // Récup de la chaine de donnees
	}
	echo '</head> <body>';
	include ("menu_insider.php"); // Menu
	echo '<span class = "small">Votre cache : '.$ip.'</span><br>';
	if(isset($bdd))
	{
		echo '
		<h3 class="titre_params">Infos sur la MasterReporting "'.$base_de_donnees.'" : </h3> <br/><br/>
		- La base contient <strong>'.$nb_dl.'</strong> connecteurs.<br/><br/>
		- Le nombre de modèles distincts parmi ces connecteurs est de <strong>'.$nb_vue_distinct.'</strong>, ils sont utilisés <strong>'.$nb_vue.'</strong> fois en tout sur l\'ensemble des connecteurs.<br/><br/>
		<h4>Version de la base et du produit :</h4>';
		while ($infos_version = $version_produit->fetch())
		{
		echo
		'<br>
		Produit : '.$infos_version['produit'].' - Version : '.$infos_version['num_version'].'<br>
		'
		;
		include("recup_logs/logs_infos_base.php"); // Ajout logs infos générales
		}
	}
	else
	{
		echo '<br><br><span class="red">Impossible de récupérer les infos de la BDD - Base de données référentielle non-trouvée</span>';
	}	
	
	

?>
	</body>
</html>