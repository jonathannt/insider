<?php
// Nb de connecteurs 
$nb_connecteurs = $bdd->query("
SELECT
COUNT(ID_DL) as id_dl
FROM
DL_CONNECTEURS
");

$nb_conn = $nb_connecteurs->fetch();

$nb_dl = $nb_conn['id_dl'];


// Nb de modèles
$nb_modeles = $bdd->query("
SELECT
COUNT(ID_DL) as id_dl
FROM
DL_VUES
");

$nb_mod = $nb_modeles->fetch();

$nb_vue = $nb_mod['id_dl'];


// Nb de modèles dinstinct
$nb_modeles_distinct = $bdd->query("
SELECT
COUNT(DISTINCT ID_VUE) as id_vue
FROM
DL_VUES
");


$nb_mod_dis = $nb_modeles_distinct->fetch();

$nb_vue_distinct = $nb_mod_dis['id_vue'];

// Version de la base
$version_produit = $bdd->query("
SELECT
PRODUCT as produit,
VERSION as num_version
FROM
VERSION_PRODUCT
");


?>