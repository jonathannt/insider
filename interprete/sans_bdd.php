<?php

//Conditions pour récupérer l'ID du connecteur

		if(substr($_POST['formule'],5,2) == 'AC' OR substr($_POST['formule'],5,2) == 'AL' OR substr($_POST['formule'],5,2) == 'SR')
		{
			$init_formule = '("';
			$fin_nom_connecteur = '__';
			$nb_carac_connecteur_debut = strpos($_POST['formule'],$init_formule);
			
			if($nb_carac_connecteur_debut <> NULL) // Si on a un axe calculé ou montant conditionnel
			{
				$nb_carac_connecteur_fin = strpos($_POST['formule'],$fin_nom_connecteur);
				$nom_connecteur = substr($_POST['formule'],($nb_carac_connecteur_debut+2), ($nb_carac_connecteur_fin-$nb_carac_connecteur_debut-2));
				$nom_du_connecteur = 'ID du connecteur : "<strong>'.$nom_connecteur.'</strong>" <br/><br/>';
			}	
			
			else
			{
				$nom_du_connecteur = '';
			}
		}
		
		else
		{
			$nom_du_connecteur = '';
		}
		
		
		// Conditions pour récupérer l'ID du modèle
		
		if(substr($_POST['formule'],5,2) == 'AC' OR substr($_POST['formule'],5,2) == 'AL')
		{
			$init_modele = ';';
			$fin_nom_modele = '@';
			$nb_carac_modele_debut = strpos($_POST['formule'],$init_modele); 
			
			if($nb_carac_modele_debut <> NULL) // Si on a un axe calculé ou montant conditionnel
			{
			$nb_carac_modele_fin = strpos($_POST['formule'],$fin_nom_modele);
			$nom_modele = substr($_POST['formule'],($nb_carac_modele_debut+1), ($nb_carac_modele_fin-$nb_carac_modele_debut-1));
			$nom_du_modele = 'ID du modèle : "<strong>'.$nom_modele.'</strong>" <br/><br/>';
			}
			
			else
			{
				$nom_du_modele = '';
			}	
		}
		
		else
		{
			$nom_du_modele = 'pas un ALI ni un ACE';
		}


		// Conditions pour les détails du champ de la formule : ACE pour l'instant

		if(substr($_POST['formule'],5,2) == 'AC')
		{
			$nb_carac_modele_debut = strpos($_POST['formule'],'L='); // Check de présence d'axe/montant
			
			// Check de présence champ classique
			$init_mod_champs = 'S=';
			$fin_nom_mod_champs = 'G=';
			$nb_carac_mod_champs_debut = strpos($_POST['formule'],$init_mod_champs); 

			if($nb_carac_modele_debut <> NULL) // Si on a un axe calculé ou montant conditionnel
			{
				$debut_axe_montant = strpos($_POST['formule'],'L=');
				$fin_axe_montant = strpos($_POST['formule'],'E=');
				$valeur_axe_montant = substr($_POST['formule'],$debut_axe_montant+2,($fin_axe_montant-3)-($debut_axe_montant)); 

				// Définition si axe calculé OU montant conditionnel
				$nature_axe_montant_debut = strpos($_POST['formule'],'Y=');
				$chaine_axe_montant = strstr($_POST['formule'],'Y=');
				$nature_axe_montant = substr($chaine_axe_montant,2,1);
				
				if ($nature_axe_montant == '1') 
				{$nature_axe_montant = 'un axe calculé';
				// Recherche la formule de l'axe calculé	
				$debut_axe_formulax = strpos($_POST['formule'],'F=');
				$fin_axe_formulax = strpos($_POST['formule'],'Y=');
				$valeur_axe_formulax = substr($_POST['formule'],$debut_axe_formulax+2,($fin_axe_formulax-3)-($debut_axe_formulax)); 
				$nom_formulax = 'Formule de l\'axe calculé (N° = ID des champs): <strong>'.$valeur_axe_formulax.'</strong><br/><br/>';
			    } 
				elseif (($nature_axe_montant == '0')) 
				{$nature_axe_montant = 'un montant conditionnel'; 
				// Recherche la formule du montant conditionnel
				$debut_axe_formulax = strpos($_POST['formule'],'F=');
				$fin_axe_formulax = strpos($_POST['formule'],'Y=');
				$valeur_axe_formulax = substr($_POST['formule'],$debut_axe_formulax+2,($fin_axe_formulax-3)-($debut_axe_formulax)); 
				$nom_formulax = 'Formule du montant conditionnel (N° = ID des champs): <strong>'.$valeur_axe_formulax.'</strong><br/><br/>';}
				
				else {$nature_axe_montant = 'un Axe calculé / Montant conditionnel'; $valeur_axe_formulax = ''; $nom_formulax = '';}
				
				$nom_axe_montant = '<span class="titre_params_rubriques">Détails champ(s) :</span> <hr>
				C\'est '.$nature_axe_montant.' du nom de "<strong>'.$valeur_axe_montant.'</strong>" <br/><br/>';
				$nom_du_mod_champs =  '';
				$nom_source_mod_champs = '';
			}
			
			else // Sinon on récupère le champ normal
			{
				$nb_carac_mod_champs_fin = strpos($_POST['formule'],$fin_nom_mod_champs);
				$nom_mod_champs = substr($_POST['formule'],($nb_carac_mod_champs_debut+2), ($nb_carac_mod_champs_fin-$nb_carac_mod_champs_debut-3));
				
				$nom_du_mod_champs = '<span class="titre_params_rubriques">Détails champ(s) :</span> <hr>
				ID du Champ (axe) : <strong>'.$nom_mod_champs.'</strong> <br/><br/>';
				$nom_axe_montant = '';
				$nom_formulax = '';
			}
			
			$nb_carac_agreg_debut = strpos($_POST['formule'],'E='); // Check de présence agrégat

			if($nb_carac_agreg_debut <> NULL) // Si on a un agrégat
			{
				$debut_axe_agreg = strpos($_POST['formule'],'E=');
				$fin_axe_agreg = strpos($_POST['formule'],'S=');
				$valeur_axe_agreg = substr($_POST['formule'],$debut_axe_agreg+2,($fin_axe_agreg-3)-($debut_axe_agreg));
				
				// Définition de la nature de l'agrégat - isolation de sa valeur
				$nature_axe_agreg_debut = strpos($_POST['formule'],'E=');
				$chaine_axe_agreg = strstr($_POST['formule'],'E=');
				$nature_axe_agreg = substr($chaine_axe_agreg,2,1);
				if ($nature_axe_agreg == '0') {$nature_axe_agreg = 'Aucun agrégat';} 
				elseif (($nature_axe_agreg == '1')) {$nature_axe_agreg = 'Somme (total des valeurs)';} 
				elseif (($nature_axe_agreg == '2')) {$nature_axe_agreg = 'Compter (nombre de valeurs)';} 
				elseif (($nature_axe_agreg == '3')) {$nature_axe_agreg = 'Moyenne (moyenne des valeurs)';} 
				elseif (($nature_axe_agreg == '4')) {$nature_axe_agreg = 'Minimum (affiche la valeur la plus basse)';} 
				elseif (($nature_axe_agreg == '5')) {$nature_axe_agreg = 'Maximum (affiche la valeur la plus haute)';}
				elseif (($nature_axe_agreg == '6')) {$nature_axe_agreg = 'Somme distincte (total distinct des valeurs)';} 
				elseif (($nature_axe_agreg == '7')) {$nature_axe_agreg = 'Moyenne distincte (moyenne distincte des valeurs)';} 
				elseif (($nature_axe_agreg == '8')) {$nature_axe_agreg = 'Compter distinct (total distinct des valeurs)';} 
				else {$nature_axe_agreg = 'agrégat inconnu';}
				$nom_axe_agreg = 'Agrégat : <strong>'.$nature_axe_agreg.'</strong><br/><br/>';
				
			}
			
			else 
			{
				$nom_axe_agreg = '';
			}
			
			
			$nb_carac_filtres_debut = strpos($_POST['formule'],'@R'); // Check de présence filtres
			
			if($nb_carac_filtres_debut <> NULL) // Si on a des filtres
			{
				$debut_axe_filtres = strpos($_POST['formule'],'@R');
				$fin_axe_filtres = strpos($_POST['formule'],':"')+1; // Fin de la formule
				$valeur_axe_filtres = substr($_POST['formule'],$debut_axe_filtres+1,($fin_axe_filtres-1)-($debut_axe_filtres));
				$nb_de_filtres = mb_substr_count($valeur_axe_filtres, ':'); // Compte le nombre de filtres		
					
				// Ordre du filtres
					
				$chaine_filtre_global1 = strstr($_POST['formule'],'R=');
				$debut_filtre1 = strpos($chaine_filtre_global1,'R');
				$fin_filtre1 = strpos($chaine_filtre_global1,':');
				$chaine_filtre1 = substr($chaine_filtre_global1, $debut_filtre1, $fin_filtre1);
				
				// Recup des arguments du premier filtre : 
					
				$chaine_champ_filtre1 = substr($chaine_filtre1,strpos($chaine_filtre1,'S')+2,(strpos($chaine_filtre1,'V'))-7);
					
						
					// Récup de la valeur du filtre 1
						
					$recup_valeur_filtre1 = strstr($chaine_filtre1,'V=');
					$valeur_filtre1 = substr($recup_valeur_filtre1,2);
						
					$nom_du_filtre1 = '<span class="titre_params_rubriques">Détails filtres :</span> <hr>
					Filtre sur ID : <strong>'.$chaine_champ_filtre1.'</strong> - Valeur : "'.$valeur_filtre1.'"<br/><br/>';		
					
					$i = 2;
					while ($i <= $nb_de_filtres) // Boucle pour segmetner les autres chaines de filtres (s'il y a)
					{
						
						// Définition chaine de chaque filtre
						${'chaine_filtre_global'.$i} = substr(strstr(${'chaine_filtre_global'.($i-1)},':'),1,strlen(strstr(${'chaine_filtre_global'.($i-1)},':')));
						${'debut_filtre'.$i} = 0;
						${'fin_filtre'.$i} = strpos(${'chaine_filtre_global'.($i)},':');
						${'chaine_filtre'.$i} = substr(${'chaine_filtre_global'.$i},${'debut_filtre'.$i},${'fin_filtre'.$i});
						
						// Définition des arguments des filtres
						²
						${'chaine_champ_filtre'.$i} = substr(${'chaine_filtre'.$i},strpos(${'chaine_filtre'.$i},'S')+2,(strpos(${'chaine_filtre'.$i},'V'))-7);
						
						// Récup de la valeur du filtre
						
						${'recup_valeur_filtre'.$i} = strstr(${'chaine_filtre'.$i},'V=');
						${'valeur_filtre'.$i} = substr(${'recup_valeur_filtre'.$i},2);
						
						// Définition finale du filtre
						
						${'nom_du_filtre'.$i} = 'Filtre sur ID : <strong>'.${'chaine_champ_filtre'.$i}.'</strong> - Valeur : "'.${'valeur_filtre'.$i}.'"<br/><br/>';		
						$i++;

					}
					
				$presence_filtres_avancees = strpos($valeur_axe_filtres, '@'); // Check présence filtre avancés
				
				if ($presence_filtres_avancees <> NULL) // Si on a des filtres avancés ou non
				{
					$chaine_filtres_avances_base = strstr($valeur_axe_filtres, '@');
					$chaine_filtres_avances = substr($chaine_filtres_avances_base,1);
					$filtres_avances = 'Filtres avancés : "'.$chaine_filtres_avances. '"<br/<br/>';
				}	
				
				else
				{
					$filtres_avances = '';
				}
				
			}
			
			else
			{
					$nb_de_filtres = 1;
					$nom_du_filtre1 = '';
					$filtres_avances = '';
			}	
			
				
		}

		elseif (substr($_POST['formule'],5,2) == 'AL')
		{
		include("ali_sans_bdd.php");
		}	

		else // Si ce n'est pas un assistant cellule (?)
		{
			$nom_axe_montant = '';
			$nom_du_mod_champs =  '';
			$nom_source_mod_champs = '';
			$nom_axe_agreg = '';
			$nom_formulax = '';
			$nom_axe_filtres = '';
			$nb_de_filtres = 1;
			$nom_du_filtre1 = '';
			$filtres_avances = '';
		}
		
		// Informations diffusées :

		echo '<span class="titre_params">Résultat :</span> <em>(sans connexion à une BDD)</em><br/><br/>
		<hr>
		<span class="titre_params_rubriques">Nature :</span>
		<hr>
		Assistant : <strong>'.$type_assistant.'</strong> <br/><br/>'
		.$nom_du_connecteur.
		$nom_du_modele.
		$nom_du_mod_champs.
		$nom_axe_montant.
		$nom_formulax.
		$nom_axe_agreg.
		$nom_du_filtre1;
		
		$j=2;
		while ($j <= $nb_de_filtres) // Boucle pour segmetner les autres chaines de filtres (s'il y a)
		{
				echo ${'nom_du_filtre'.$j};
				$j++;
		}	
		
		echo
		$filtres_avances.
		'<br/>
		</article>';
?>