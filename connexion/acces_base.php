<?php 
/**
 * Récupérer la véritable adresse IP d'1 visiteur
 */
$ip = gethostbyaddr($_SERVER['REMOTE_ADDR']);
// Connexion à la base de données

$serveur = $_POST['serveur'];
$base_de_donnees = $_POST['bdd'];
$utilisateur = $_POST['user'];
$mot_de_passe = $_POST['mdp'];

try
{
    $bdd = new PDO("sqlsrv:Server=".$serveur.";Database=".$base_de_donnees."", $utilisateur , $mot_de_passe);
	$connexion = 'ok';
	include("recup_logs/logs_acces_base.php"); // Ajout logs accès base
}



catch(Exception $e)
{
        $connexion = 'ko';
		include("recup_logs/logs_acces_base.php"); // Ajout logs accès base
		echo '<h4 class="red">ERREUR DE CONNEXION A LA BASE DE DONNÉES</h4> <strong>- Nature de l\'erreur :</strong><br><br>';
		die($e->getMessage());
}
// Création du cache (1 cache par machine qui se connecte) LOL
$info_connexion = fopen('connexion/info_connexion'.$ip.'.php', 'w+');
fputs($info_connexion, '<?php $bdd = new PDO("sqlsrv:Server='.$serveur.';Database='.$base_de_donnees.'", "'.$utilisateur.'" , "'.$mot_de_passe.'"); ?>');
fputs($info_connexion, "\r\n");
fclose($info_connexion);

$info_connexion_chaine = fopen('connexion/info_connexion_chaine'.$ip.'.php', 'w+');
fputs($info_connexion_chaine, '<?php $base_de_donnees=\''.$base_de_donnees.'\'?>');
fputs($info_connexion_chaine, "\r\n");
fclose($info_connexion_chaine);



?>