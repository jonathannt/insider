<?php

// Définition des champs
$formule_sans_reliures = str_replace('"&"','',$_POST['formule']);
$formule_sans_reliures = str_replace('=@R','=R',$formule_sans_reliures); // Pour gérer les nouvelles formules =@
$chaine_champ_sans_fin_avec_filtres = strstr($formule_sans_reliures,'@');
$longueur_chaine_sans_fin = strlen($chaine_champ_sans_fin_avec_filtres);
//$chaine_champ_rework_sans_arobase_debut = substr($chaine_champ_sans_fin_avec_filtres,1,$longueur_chaine_sans_fin);
$position_chaine_champ_sans_fin = strpos($chaine_champ_sans_fin_avec_filtres,'@R');

if($position_chaine_champ_sans_fin <> NULL) // Si on a bien des filtres
{
	$chaine_champs = substr($chaine_champ_sans_fin_avec_filtres,0,$position_chaine_champ_sans_fin+1); // On génère la partie de la formule sur les champs en prenant en compte les filtres en fin de formule
	$check_filtres = 'yes'; // Variable validant ou non la présence de filtres, pour le parsing
}
else
{
	$position_chaine_champ_sans_fin = strrpos($chaine_champ_sans_fin_avec_filtres,'")'); // On redéfinie la position finale
	$chaine_champs = substr($chaine_champ_sans_fin_avec_filtres,0,$position_chaine_champ_sans_fin+1); // On génère la partie de la formule sur les champs sans filtre pris en compte
	$check_filtres = 'no'; // Variable validant ou non la présence de filtres, pour le parsing
}	

// Création d'une boucle pour chaque champ

// DEFINITION CHAINE DE CHAQUE CHAMP DE L'ACU

$nb_de_champs_acu = mb_substr_count($formule_sans_reliures, 'L=');

$nb_de_champs_final = '<span class="titre_params_rubriques">Détails champ(s) :</span> <hr> Nombre de champ(s) : <strong>'.$nb_de_champs_acu.'</strong><br/><br/>';

	
// REECRITURE ALGO
$chaine_champ_acu_global0 = substr($chaine_champs,1); // Chaines contenant les champs + Filtres, sans le symbole @ du début
		
// TRONCAGE DES CHAINES
		
// Troncage chaine de colonnes
$chaine_colonnes = substr($chaine_champ_acu_global0,0,strpos($chaine_champ_acu_global0,'@')+1);
		
// Troncage chaine de lignes
$chaine_lignes_prep = substr($chaine_champ_acu_global0,strlen($chaine_colonnes));
$chaine_lignes = substr($chaine_lignes_prep,0,strpos($chaine_lignes_prep,'@')+1);
		
// Troncage chaine de mesure
if($check_filtres == 'yes') // Si on a des filtres, la fin est un @
{$fin_mesure = '@';}
else
{$fin_mesure = '\'"';}
$chaine_mesures_prep = substr($chaine_champ_acu_global0,strlen($chaine_colonnes)+strlen($chaine_lignes));
$chaine_mesures = substr($chaine_mesures_prep,0,strpos($chaine_mesures_prep,$fin_mesure)+2);
		
// NOMBRE DE CHAMPS PAR CATÉGORIES
		
$nombre_colonnes = mb_substr_count($chaine_colonnes,':')+1; // On compte le nombre de symboles servant d'intermédiaires (ici deux-points). S'il n'y en a pas, c'est qu'on a qu'une colonnes/lignes/mesures
$nombre_lignes = mb_substr_count($chaine_lignes,':')+1;
$nombre_mesures= mb_substr_count($chaine_mesures,':')+1;
		
// MISE EN PLACE DE LA BOUCLE PAR TYPE DE CHAMPS, POUR EXTRAIRE UNE CHAINE PAR CHAMP

// Boucle des colonnes : 

$c = 1;
while ($c <= $nombre_colonnes)
{
	if($c === 1) // Si on est à la 1ère colonne, on récupère la chaine différement + on ajute la Nature du champ
	{
		${'nature_champ_cube_colonnes'.$c} = '<span class="titre_params_cube">Colonne(s) :</span><br/>';
		$chaine_colonnes1 = $chaine_colonnes;
		if ($nombre_colonnes > 1) // Si l'on a plus d'une colonne, on ramènera le premier champ différemment
		{ ${'chaine_champ_colonne'.$c} = substr($chaine_colonnes,0,strpos($chaine_colonnes,':')); }
		else
		{ ${'chaine_champ_colonne'.$c} = substr($chaine_colonnes,0,strpos($chaine_colonnes,'@')); }
	}
	
	else
	{
		${'nature_champ_cube_colonnes'.$c} = ''; // On enlève le titre indiquant les colonnes (voir plus haut)
		${'chaine_colonnes'.$c} = substr(${'chaine_colonnes'.($c-1)},strlen(${'chaine_champ_colonne'.($c-1)})+1); // Chaine des colonnes moins le champ précédent
		if ($c < $nombre_colonnes) // Si on est pas à la dernière colonne (et pas la 1ère non plus, voir plus haut)
		{ ${'chaine_champ_colonne'.$c} = substr(${'chaine_colonnes'.$c},0,strpos(${'chaine_colonnes'.$c},':')); }
		else
		{ ${'chaine_champ_colonne'.$c} = substr(${'chaine_colonnes'.$c},0,strpos(${'chaine_colonnes'.$c},'@')); }
	}
	$c++;
}

// Boucle des lignes

$l = 1;
while ($l <= $nombre_lignes)
{
	if($l === 1) // Si on est à la 1ère ligne, on récupère la chaine différement + on ajute la Nature du champ
	{
		${'nature_champ_cube_lignes'.$l} = '<span class="titre_params_cube">Ligne(s) :</span><br/>';
		$chaine_lignes1 = $chaine_lignes;
		if ($nombre_lignes > 1) // Si l'on a plus d'une ligne, on ramènera le premier champ différemment
		{ ${'chaine_champ_ligne'.$l} = substr($chaine_lignes1,0,strpos($chaine_lignes1,':')); }
		else
		{ ${'chaine_champ_ligne'.$l} = substr($chaine_lignes1,0,strpos($chaine_lignes1,'@')); }
	}
	
	else
	{
		${'nature_champ_cube_lignes'.$l} = ''; // On enlève le titre indiquant les lignes (voir plus haut)
		${'chaine_lignes'.$l} = substr(${'chaine_lignes'.($l-1)},(strlen(${'chaine_champ_ligne'.($l-1)}))+1); // Chaine des lignes moins le champ précédent
		if ($l < $nombre_lignes) // Si on est pas à la dernière ligne (et pas la 1ère non plus, voir plus haut)
		{ ${'chaine_champ_ligne'.$l} = substr(${'chaine_lignes'.$l},0,strpos(${'chaine_lignes'.$l},':')); }
		else
		{ ${'chaine_champ_ligne'.$l} = substr(${'chaine_lignes'.$l},0,strpos(${'chaine_lignes'.$l},'@')); }
	}
	$l++;
}

// Boucle des mesures

$m = 1;
while ($m <= $nombre_mesures)
{

	if($m === 1) // Si on est à la 1ère mesure, on récupère la chaine différement + on ajute la Nature du champ
	{
		${'nature_champ_cube_mesures'.$m} = '<span class="titre_params_cube">Mesure(s) :</span><br/>';
		$chaine_mesures1 = $chaine_mesures;
		if ($nombre_mesures > 1) // Si l'on a plus d'une ligne, on ramènera le premier champ différemment
		{ ${'chaine_champ_mesure'.$m} = substr($chaine_mesures,0,strpos($chaine_mesures,':')); }
		else 
		{ ${'chaine_champ_mesure'.$m} = substr($chaine_mesures,0,strpos($chaine_mesures,$fin_mesure)); }
	}
	
	else
	{
		${'nature_champ_cube_mesures'.$m} = ''; // On enlève le titre indiquant les mesures (voir plus haut)
		${'chaine_mesures'.$m} = substr(${'chaine_mesures'.($m-1)},strlen(${'chaine_champ_mesure'.($m-1)})+1); // Chaine des mesures moins le champ précédent
		if ($m < $nombre_mesures) // Si on est pas à la dernière ligne (et pas la 1ère non plus, voir plus haut)
		{ ${'chaine_champ_mesure'.$m} = substr(${'chaine_mesures'.$m},0,strpos(${'chaine_mesures'.$m},':')); }
		else
		{ ${'chaine_champ_mesure'.$m} = substr(${'chaine_mesures'.$m},0,strpos(${'chaine_mesures'.$m},$fin_mesure)); }
	}
	$m++;
}

	
	
$i = 1;  // BOUCLE GLOBALE : Boucle pour segmenter les chaines de champs ACU
while ($i <= $nb_de_champs_acu)
{

	// NOUVEL ALGO CHAINES DE CHAMPS : On redéfinit les chaines champs acu grâce au nouvel algo plus haut

	if($i <= $nombre_lignes) // LIGNES
	{ 
		${'nature_champ_cube'.$i} = ${'nature_champ_cube_lignes'.$i}; 
		${'chaine_champ_acu'.$i} = ${'chaine_champ_ligne'.$i};
	}
	elseif($i > $nombre_lignes AND $i <= $nombre_colonnes+$nombre_lignes) // COLONNES
	{ 
		${'nature_champ_cube'.$i} = ${'nature_champ_cube_colonnes'.($i-$nombre_lignes)}; 
		${'chaine_champ_acu'.$i} = ${'chaine_champ_colonne'.($i-$nombre_lignes)};
	}
	else // MESURES
	{ 
		${'nature_champ_cube'.$i} = ${'nature_champ_cube_mesures'.($i-($nombre_lignes+$nombre_colonnes))}; 
		${'chaine_champ_acu'.$i} = ${'chaine_champ_mesure'.($i-($nombre_lignes+$nombre_colonnes))};
	}	
	// AGREGATIONS
	
	// Récup de l'agreg du champ OU du total si pas dans "mesures"
	${'check_agreg'.$i} = strpos(${'chaine_champ_acu'.$i},'L='); // Si axe calculé/montant cond., on ramène l'agreg différemment
	if(${'check_agreg'.$i} == 0 AND strpos(${'chaine_champ_acu'.$i},'G=') == 0) // S'il y a bien un axe en MESURE
	{
		${'longueur_difference_agreg'.$i} = (strpos(${'chaine_champ_acu'.$i},'F=')-1)-(strpos(${'chaine_champ_acu'.$i},'E=')+2);
		${'valeur_agreg_champ_check'.$i} = substr(${'chaine_champ_acu'.$i},strpos(${'chaine_champ_acu'.$i},'E=')+2,${'longueur_difference_agreg'.$i});
	}	
	elseif(${'check_agreg'.$i} == 0 AND strpos(${'chaine_champ_acu'.$i},'G=') <> 0) // S'il y a bien un axe en LIGNE OU EN COLONNE
	{
		${'longueur_difference_agreg'.$i} = (strpos(${'chaine_champ_acu'.$i},'F=')-1)-(strpos(${'chaine_champ_acu'.$i},'C=')+2);
		${'valeur_agreg_champ_check'.$i} = substr(${'chaine_champ_acu'.$i},strpos(${'chaine_champ_acu'.$i},'C=')+2,${'longueur_difference_agreg'.$i});
	}	
	else // Sinon on ramène l'agragat de manière classique
	{
		if(strpos(${'chaine_champ_acu'.$i},'G=') == NULL) // Si on est sur une MESURE
		{
			${'longueur_difference_agreg'.$i} = (strpos(${'chaine_champ_acu'.$i},'S=')-1)-(strpos(${'chaine_champ_acu'.$i},'E=')+2);
			${'valeur_agreg_champ_check'.$i} = substr(${'chaine_champ_acu'.$i},strpos(${'chaine_champ_acu'.$i},'E=')+2,${'longueur_difference_agreg'.$i});
		}
		else // Si on est sur une COLONNE ou une LIGNE
		{	
			${'longueur_difference_agreg'.$i} = (strpos(${'chaine_champ_acu'.$i},'L=')-1)-(strpos(${'chaine_champ_acu'.$i},'C=')+2);
			${'valeur_agreg_champ_check'.$i} = substr(${'chaine_champ_acu'.$i},strpos(${'chaine_champ_acu'.$i},'C=')+2,${'longueur_difference_agreg'.$i});
		}
	}
	
	${'valeur_agreg_champ'.$i} = ${'valeur_agreg_champ_check'.$i};
	
	if(is_numeric(strpos(${'chaine_champ_acu'.$i},'G='))) // Si on est sur une LIGNE ou COLONNE
	{
		if (${'valeur_agreg_champ'.$i} == '0') {${'valeur_agreg_champ'.$i} = 'Somme';} 
		elseif (${'valeur_agreg_champ'.$i} == '1') {${'valeur_agreg_champ'.$i} = 'Différence';} 
		elseif (${'valeur_agreg_champ'.$i} == '2') {${'valeur_agreg_champ'.$i} = 'Aucun';} 
		elseif (${'valeur_agreg_champ'.$i} == '3') {${'valeur_agreg_champ'.$i} = 'Compter';} 
		elseif (${'valeur_agreg_champ'.$i} == '4') {${'valeur_agreg_champ'.$i} = 'Moyenne';} 
		elseif (${'valeur_agreg_champ'.$i} == '5') {${'valeur_agreg_champ'.$i} = 'Maximum';}
		elseif (${'valeur_agreg_champ'.$i} == '6') {${'valeur_agreg_champ'.$i} = 'Minimum';} 
		${'nom_agreg_champ'.$i} = ' - Total : <strong>'.${'valeur_agreg_champ'.$i}.'</strong>';
		$check_mesure = 'nok';
	}		
	
	else
	{	
		if (${'valeur_agreg_champ'.$i} == '0') {${'valeur_agreg_champ'.$i} = 'Aucun';} 
		elseif (${'valeur_agreg_champ'.$i} == '1') {${'valeur_agreg_champ'.$i} = 'Somme';} 
		elseif (${'valeur_agreg_champ'.$i} == '2') {${'valeur_agreg_champ'.$i} = 'Compter';} 
		elseif (${'valeur_agreg_champ'.$i} == '3') {${'valeur_agreg_champ'.$i} = 'Moyenne';} 
		elseif (${'valeur_agreg_champ'.$i} == '4') {${'valeur_agreg_champ'.$i} = 'Maximum';} 
		elseif (${'valeur_agreg_champ'.$i} == '5') {${'valeur_agreg_champ'.$i} = 'Minimum';}
		elseif (${'valeur_agreg_champ'.$i} == '6') {${'valeur_agreg_champ'.$i} = 'Somme distincte';} 
		elseif (${'valeur_agreg_champ'.$i} == '7') {${'valeur_agreg_champ'.$i} = 'Moyenne distincte';} 
		elseif (${'valeur_agreg_champ'.$i} == '8') {${'valeur_agreg_champ'.$i} = 'Compter distinct';} 
		elseif (${'valeur_agreg_champ'.$i} == '9') {${'valeur_agreg_champ'.$i} = 'Somme non-totalisée';} 
		elseif (${'valeur_agreg_champ'.$i} == '10') {${'valeur_agreg_champ'.$i} = 'Compter non-totalisé';} 
		elseif (${'valeur_agreg_champ'.$i} == '11') {${'valeur_agreg_champ'.$i} = 'Moyenne non-totalisée';} 
		elseif (${'valeur_agreg_champ'.$i} == '12') {${'valeur_agreg_champ'.$i} = '% du Total ligne';} 
		elseif (${'valeur_agreg_champ'.$i} == '13') {${'valeur_agreg_champ'.$i} = '% du Total colonne';} 
		elseif (${'valeur_agreg_champ'.$i} == '14') {${'valeur_agreg_champ'.$i} = '% de la Ligne parente';} 
		elseif (${'valeur_agreg_champ'.$i} == '15') {${'valeur_agreg_champ'.$i} = '% de la Colonne parente';} 
		elseif (${'valeur_agreg_champ'.$i} == '16') {${'valeur_agreg_champ'.$i} = '% du Grand Total';} 
		elseif (${'valeur_agreg_champ'.$i} == '17') {${'valeur_agreg_champ'.$i} = 'Différence en valeur';} 
		elseif (${'valeur_agreg_champ'.$i} == '18') {${'valeur_agreg_champ'.$i} = 'Différence en pourcentage';}
		else {${'valeur_agreg_champ'.$i} = 'agrégat inconnu (ID retrouvé : '.${'valeur_agreg_champ_check'.$i} .')';}
		${'nom_agreg_champ'.$i} = ' - Agrégat : <strong>'.${'valeur_agreg_champ'.$i}.'</strong>';
		
	}
	
	// CHAMPS
	// Récup du nom MC du champ 
	// Vérification si axe calculé / Montant conditionnel

	
	
	${'check_axe'.$i} = strpos(${'chaine_champ_acu'.$i}, 'Y=');
	if(${'check_axe'.$i} <> NULL)
	{
		${'debut_axe_montant'.$i} = strpos(${'chaine_champ_acu'.$i},'L=');
					${'fin_axe_montant'.$i} = strpos(${'chaine_champ_acu'.$i},'E=');
					${'valeur_axe_montant'.$i} = substr(${'chaine_champ_acu'.$i},${'debut_axe_montant'.$i} +2,(${'fin_axe_montant'.$i}-3)-(${'debut_axe_montant'.$i} )); 

					// Définition si axe calculé OU montant conditionnel
					${'nature_axe_montant_debut'.$i} = strpos(${'chaine_champ_acu'.$i},'Y=');
					${'chaine_axe_montant'.$i} = strstr(${'chaine_champ_acu'.$i},'Y=');
					${'nature_axe_montant'.$i}= substr(${'chaine_axe_montant'.$i},2,1);
					
					// Récupération de la formule d'un axe calculé
					
					if (${'nature_axe_montant'.$i} == '1') 
					{${'nature_axe_montant'.$i} = '<strong>Axe calculé</strong>';
					// Recherche la formule de l'axe calculé	
					${'debut_axe_formulax'.$i}  = strpos(${'chaine_champ_acu'.$i},'F=');
					${'fin_axe_formulax'.$i}  = strpos(${'chaine_champ_acu'.$i},'Y=');
					${'valeur_axe_formulax'.$i}  = substr(${'chaine_champ_acu'.$i},${'debut_axe_formulax'.$i} +2,(${'fin_axe_formulax'.$i} -3)-(${'debut_axe_formulax'.$i} )); 
					${'nom_formulax'.$i}  = ' <strong>'.${'valeur_axe_formulax'.$i} .'</strong></em>';
					 } 
					elseif ((${'nature_axe_montant'.$i} == '0')) 
					{${'nature_axe_montant'.$i} = '<strong>Montant conditionnel</strong>'; 
					// Recherche la formule du montant conditionnel
					${'debut_axe_formulax'.$i}   = strpos(${'chaine_champ_acu'.$i},'F=');
					${'fin_axe_formulax'.$i} = strpos(${'chaine_champ_acu'.$i},'Y=');
					${'valeur_axe_formulax'.$i} = substr(${'chaine_champ_acu'.$i},${'debut_axe_formulax'.$i}  +2,(${'fin_axe_formulax'.$i}-3)-(${'debut_axe_formulax'.$i}  )); 
					${'nom_formulax'.$i} = ' <strong>'.${'valeur_axe_formulax'.$i}.'</strong></em>';}
					else {${'nature_axe_montant'.$i} = 'Axe calculé / Montant conditionnel'; ${'valeur_axe_formulax'.$i} = ''; ${'nom_formulax'.$i} = '';}
					${'nom_detail_champ'.$i} = ${'nature_champ_cube'.$i}.'Champ '.$i.' : 
					'.${'nature_axe_montant'.$i}.' du nom de "<strong>'.${'valeur_axe_montant'.$i}.'</strong>" '.${'nom_agreg_champ'.$i}.' - <em>Formule : '.${'nom_formulax'.$i}.'<br/>';
					${'nom_du_mod_champs'.$i} =  '';
					${'nom_source_mod_champs'.$i} = '';
	}	

	 else
	{
		if(strpos(${'chaine_champ_acu'.$i},'G=') == 0) // Si c'est une mesure (pas une colonne ni une ligne)
		{
			${'longueur_difference'.$i} = (strpos(${'chaine_champ_acu'.$i},'L=')-1)-(strpos(${'chaine_champ_acu'.$i},'S=')+2); // Calcul de la longueur de l'ID
		}
		else
		{
			${'longueur_difference'.$i} = (strpos(${'chaine_champ_acu'.$i},'G=')-1)-(strpos(${'chaine_champ_acu'.$i},'S=')+2); // Calcul de la longueur de l'ID
		}
	${'id_champ_mc_base'.$i} = substr(${'chaine_champ_acu'.$i},strpos(${'chaine_champ_acu'.$i},'S=')+2,strlen(substr(${'chaine_champ_acu'.$i},strpos(${'chaine_champ_acu'.$i},'S=')+2,${'longueur_difference'.$i}))); // Récup de l'ID du champ
	${'id_champ_mc'.$i} = ${'id_champ_mc_base'.$i};

	if(is_numeric(${'id_champ_mc'.$i})) // Débogage
		{

			// Requête pour récupérer le nom du champ
			${'requete_nom_champ'.$i} = $bdd->query("
							SELECT TOP 1
							ID_MC as id_mc
							,DESIGNATION as desig
							,SOURCE as source
							FROM
							DL_MODELES_CHAMPS
							WHERE
							ID_VUE = '".$nom_modele."' AND ID_DL = '".$nom_connecteur."' AND ID_MC = '".${'id_champ_mc'.$i}."'
							");
			${'fetch_nom_champ'.$i} = ${'requete_nom_champ'.$i}->fetch();
			${'id_champ'.$i} = ${'fetch_nom_champ'.$i}['id_mc'];
			${'nom_champ'.$i} = ${'fetch_nom_champ'.$i}['desig'];
			${'source_champ'.$i} = ${'fetch_nom_champ'.$i}['source'];
			
			if(strlen(${'nom_champ'.$i}) === 0)
			{
				${'nom_detail_champ'.$i} = 'Champ '.$i.' inconnu (pas sur la base - ID : '.${'id_champ_mc'.$i}.') '.${'nom_agreg_champ'.$i}.'<br>';
			}
			
			else
			{	
				if(strpos(${'chaine_champ_acu'.$i},'G=') == 0) // Si on est sur une MESURE
				{
					${'nom_detail_champ'.$i} = ${'nature_champ_cube'.$i}.'Champ '.$i.': <a title="<strong>ID du champ :</strong> '.${'id_champ'.$i}.'<br><strong>Source :</strong> '.str_replace('"','',${'source_champ'.$i}).' " rel="tooltip"><strong>'.${'nom_champ'.$i}.'</strong></a>'.${'nom_agreg_champ'.$i}.'<br>';
				}
				else // Si on est sur une ligne ou colonne
				{
					${'nom_detail_champ'.$i} = ${'nature_champ_cube'.$i}.'Champ '.$i.' : <a title="<strong>ID du champ :</strong> '.${'id_champ'.$i}.'<br><strong>Source :</strong> '.str_replace('"','',${'source_champ'.$i}).' " rel="tooltip"><strong>'.${'nom_champ'.$i}.'</strong></a>'.${'nom_agreg_champ'.$i}.'<br>';
				}	
			}	
		}
		
	else // Si pas de correspondance en base on note un message d'erreur ou on vérifie si c'est un dossier virtuel
		{	
			${'id_champ_mc_virtuel'.$i} = strpos(${'id_champ_mc'.$i}, '|'); // Pour dossier virtuel
			if(${'id_champ_mc_virtuel'.$i} <> NULL) // Si le champ est bien un dossier virtuel
			{
				${'id_champ_mc_virtuel_dossier'.$i} = substr(${'id_champ_mc'.$i},0,${'id_champ_mc_virtuel'.$i}); // Récup de l'ID du dossier virtuel
				${'id_champ_mc_virtuel_champ'.$i} = substr(${'id_champ_mc'.$i},${'id_champ_mc_virtuel'.$i}+1,strlen(${'id_champ_mc'.$i})); // Récup de l'ID MC du dossier virtuel
				if(is_numeric(${'id_champ_mc_virtuel_champ'.$i})) // Débogage
				{
					// Requête pour récupérer le nom du champ virtuel
					${'requete_nom_champ_virtuelle'.$i} = $bdd->query("
							SELECT 
							  DL_MODELES_CHAMPS.DESIGNATION as desig
							  ,DL_VUES.DESIGNATION as desig_vue
							  FROM DL_MODELES_CHAMPS
							  LEFT JOIN DL_VUES
							  ON
							  DL_VUES.ID_DL = DL_MODELES_CHAMPS.ID_DL 
							  AND
							  DL_VUES.ID_VUE = DL_MODELES_CHAMPS.ID_VUE
							  LEFT JOIN DL_VUES_VIRTUELLES
							  ON
							  DL_MODELES_CHAMPS.ID_DL = DL_VUES_VIRTUELLES.ID_DL
							  AND
							  DL_MODELES_CHAMPS.ID_VUE = DL_VUES_VIRTUELLES.ID_VUE_VIRTUELLE
							  WHERE 
							  DL_MODELES_CHAMPS.ID_MC = '".${'id_champ_mc_virtuel_champ'.$i}."'
							  AND
							  DL_MODELES_CHAMPS.ID_DL = '".$nom_connecteur."'
							  AND
							  DL_VUES_VIRTUELLES.ID = '".${'id_champ_mc_virtuel_dossier'.$i}."'
								");
					${'fetch_nom_champ_virtuelle'.$i} = ${'requete_nom_champ_virtuelle'.$i}->fetch();
					${'nom_champ'.$i} = ${'fetch_nom_champ_virtuelle'.$i}['desig'];
					${'nom_vue_virtuelle'.$i} = ${'fetch_nom_champ_virtuelle'.$i}['desig_vue'];
					
					if(strlen(${'nom_champ'.$i}) === 0)
					{
						${'nom_detail_champ'.$i} = ${'nature_champ_cube'.$i}.'Champ '.$i.' inconnu (virtualisé - pas sur la base - ID : '.${'id_champ_mc'.$i}.') '.${'nom_agreg_champ'.$i}.'<br>';
					}

					else
					{	
						${'nom_detail_champ'.$i} = ${'nature_champ_cube'.$i}.'Champ '.$i.' : <strong>'.${'nom_champ'.$i}.'</strong> <em>(Champ virtualisé - Modèle d\'origine : '.${'nom_vue_virtuelle'.$i}.')</em>'.${'nom_agreg_champ'.$i}.'<br/>';
					}
				}
				else
				{
					${'nom_detail_champ'.$i} = ${'nature_champ_cube'.$i}.'Champ '.$i.' inconnu (virtualisé - pas sur la base - ID : '.${'id_champ_mc'.$i}.') '.${'nom_agreg_champ'.$i}.'<br>';
				}
				
			}
				
			else // Si ce n'est pas un champ normal ou virtuel, on inscrit le message d'erreur
			{
				${'nom_detail_champ'.$i} = ${'nature_champ_cube'.$i}.'Champ '.$i.' introuvable :<em> Rapport =  ID MC is '.${'id_champ_mc'.$i}.'</em><br/>';
			}	
		}


    }
	$i++;
}

$nb_carac_filtres_debut = strpos($formule_sans_reliures,'@R'); // Check de présence filtres

// Mise en place pour filtres avec reF. cellule (s'il y a)
$chaine_filtres_mep = strstr($formule_sans_reliures,'@R'); // Précaution pour commencer la boucle des ref. cellule à la fin de la formule
$chaine_ref_cellule_mep = strstr($chaine_filtres_mep,'";');
$fin_chaine_ref_cellule = strpos($chaine_ref_cellule_mep, ')');
$chaine_ref_cellule_brut = substr($chaine_ref_cellule_mep,strlen($chaine_ref_cellule_mep)-$fin_chaine_ref_cellule);
$chaine_ref_cellule = str_replace(')','',str_replace('"','',$chaine_ref_cellule_brut));
if(strlen($chaine_ref_cellule) > 0) // S'il y a bien des ref aux cellules dans les filtres
{
	$nb_de_ref_cellule = substr_count($chaine_ref_cellule,';');
	$q =1;
	while ($q <= $nb_de_ref_cellule) // Boucle pour segmenter les ref. cellules
	{
		if($q === 1) // Si on est sur la première ref, on définit la première chaine
		{
		${'cellule_ref_brut'.$q} = $chaine_ref_cellule;
		}	
		else
		{
			${'cellule_ref_brut'.$q} = substr(${'cellule_ref_sans_fin'.($q-1)},strpos(${'cellule_ref_sans_fin'.($q-1)},';'));
		}
	
		${'cellule_ref_sans_fin'.$q} = substr(${'cellule_ref_brut'.$q},1);
		${'cellule_ref_fin'.$q} = strpos(${'cellule_ref_sans_fin'.$q},';');
		if(${'cellule_ref_fin'.$q} <> NULL)
		{	
			${'cellule_ref'.$q} = substr(${'cellule_ref_sans_fin'.$q},0,${'cellule_ref_fin'.$q});
		}
		else
		{
			${'cellule_ref'.$q} = ${'cellule_ref_sans_fin'.$q};
		}
		
		if(strlen(${'cellule_ref'.$q}) >= 4 AND substr_count(${'cellule_ref'.$q},'$') === 2) // Si la ref. cellule est invariante
		{
			${'cellule_ref_filtres'.$q} = 'Cellule '.str_replace('$','',${'cellule_ref'.$q}). ' (Invariant)';
		}	
		elseif(substr(${'cellule_ref'.$q},0,1) === '$' AND substr_count(${'cellule_ref'.$q},'$') === 1) // Si la ref. cellule est variante en ligne uniquement
		{	
			${'cellule_ref_filtres'.$q} = 'Cellule '.str_replace('$','',${'cellule_ref'.$q}). ' (Variant en ligne)';
		}
		elseif(substr_count(${'cellule_ref'.$q},'$') === 0) // Si la ref. cellule est variante en colonnes uniquement
		{
			${'cellule_ref_filtres'.$q} = 'Cellule '.str_replace('$','',${'cellule_ref'.$q}). ' (Variant en ligne et en colonne)';
		}	
		else
		{
			${'cellule_ref_filtres'.$q} = 'Cellule '.str_replace('$','',${'cellule_ref'.$q}). ' (Variant en colonne)';
		}	
		
		$q++;
	}	
}


			
	if($nb_carac_filtres_debut <> NULL) // Si on a des filtres
	{
		$debut_axe_filtres = strpos($formule_sans_reliures,'@R');
		$presence_filtres_avancees = strpos(strstr($formule_sans_reliures,'@R'), ':@'); // Check présence filtre avancés
				
		if ($presence_filtres_avancees <> NULL) // Si on a des filtres avancés ou non, on adapte la fin axe filtres
		{
			$fin_axe_filtres = strpos($formule_sans_reliures,'\'@')+1; // Fin de la formule
		}
		else
		{
			$fin_axe_filtres = strpos($formule_sans_reliures,':"')+1; // Fin de la formule
		}	
				
		$valeur_axe_filtres = substr($formule_sans_reliures,$debut_axe_filtres+1,(($fin_axe_filtres)-1)-($debut_axe_filtres)); // FAUX MAIS FONCTIONNE - NE DEVRAIT PAS NON PLUS ETRE UTILISÉ POUR LES FILTRES AVANCÉS (plus bas), à refaire si le temps
		$nb_de_filtres = mb_substr_count($valeur_axe_filtres, 'R='); // Compte le nombre de filtres
		
			$p = 1;
			while ($p <= $nb_de_filtres) // Boucle pour segmenter les autres chaines de filtres (s'il y a)
			{
				if($p === 1) // Si on est au premier filtre, le traitement est différent
				{
					// Ordre du filtres
					
					${'chaine_filtre_global'.$p} = strstr($formule_sans_reliures,'R=');
					${'debut_filtre'.$p} = strpos(${'chaine_filtre_global'.$p},'R');
					${'fin_filtre'.$p} = strpos(${'chaine_filtre_global'.$p},':');
					${'chaine_filtre'.$p} = substr(${'chaine_filtre_global'.$p}, ${'debut_filtre'.$p}, ${'fin_filtre'.$p});
							
					// Recup des arguments du premier filtre : 
								
					${'chaine_champ_filtre'.$p} = substr(${'chaine_filtre'.$p},strpos(${'chaine_filtre'.$p},'S=')+2,(strpos(${'chaine_filtre'.$p},'V='))-7);
				}	
						
				else
				{
					// Définition chaine de chaque filtre
					${'chaine_filtre_global'.$p} = substr(strstr(${'chaine_filtre_global'.($p-1)},':'),1,strlen(strstr(${'chaine_filtre_global'.($p-1)},':')));
					${'debut_filtre'.$p} = 0;
					${'fin_filtre'.$p} = strpos(${'chaine_filtre_global'.$p},':');
					${'chaine_filtre'.$p} = substr(${'chaine_filtre_global'.$p},${'debut_filtre'.$p},${'fin_filtre'.$p});
				}	
						
				// Définition des arguments des filtres
						
				${'chaine_champ_filtre'.$p} = substr(${'chaine_filtre'.$p},strpos(${'chaine_filtre'.$p},'S=')+2,(strpos(${'chaine_filtre'.$p},'V='))-7);
						
				if(is_numeric(${'chaine_champ_filtre'.$p} )) // Débogage
				{
						
				// Requête pour récupérer le champ du filtre
				${'nom_reel_filtre_champ'.$p} = $bdd->query("
				SELECT TOP 1
				DESIGNATION as desig
				FROM
				DL_MODELES_CHAMPS
				WHERE
				ID_VUE = '".$nom_modele."' AND ID_DL = '".$nom_connecteur."' AND ID_MC = '".${'chaine_champ_filtre'.$p}."'
				");
						
				${'nom_filtre_champ_reel'.$p} = ${'nom_reel_filtre_champ'.$p}->fetch();
				${'nom_filtre_champ'.$p} = ${'nom_filtre_champ_reel'.$p}['desig'];
				
				if(strlen(${'nom_filtre_champ'.$p}) === 0)
				{
					${'nom_filtre_champ'.$p} = 'Filtre '.$p.' inconnu (pas sur la base - <em>ID remonté : '.${'chaine_champ_filtre'.$p}.'</em>)';
				}		
				// Récup de la valeur du filtre
						
				${'recup_valeur_filtre'.$p} = strstr(${'chaine_filtre'.$p},'V=');
				${'valeur_filtre'.$p} = substr(${'recup_valeur_filtre'.$p},2);
						
				}
						
				else // SI l'ID du champ filtre n'est pas numérique, on vérifie si ce n'est pas un champ virtualisé, sinon on créé un ligne de débogage
				{
					${'chaine_champ_filtre_virtuel'.$p} = strpos(${'chaine_champ_filtre'.$p}, '|'); // Pour dossier virtuel
					if(${'chaine_champ_filtre_virtuel'.$p} <> NULL) // Si le champ est bien un dossier virtuel
					{
						${'chaine_champ_filtre_virtuel_dossier'.$p} = substr(${'chaine_champ_filtre'.$p},0,${'chaine_champ_filtre_virtuel'.$p}); // Récup de l'ID du dossier virtuel
						${'chaine_champ_filtre_champ'.$p} = substr(${'chaine_champ_filtre'.$p},${'chaine_champ_filtre_virtuel'.$p}+1,strlen(${'chaine_champ_filtre'.$p})); // Récup de l'ID MC du dossier virtuel
						// Requête pour récupérer le nom du champ virtuel
						${'requete_champ_filtre_virtuelle'.$p} = $bdd->query("
								SELECT 
								  DL_MODELES_CHAMPS.DESIGNATION as desig
								  ,DL_VUES.DESIGNATION as desig_vue
								  FROM DL_MODELES_CHAMPS
								  LEFT JOIN DL_VUES
								  ON
								  DL_VUES.ID_DL = DL_MODELES_CHAMPS.ID_DL 
								  AND
								  DL_VUES.ID_VUE = DL_MODELES_CHAMPS.ID_VUE
								  LEFT JOIN DL_VUES_VIRTUELLES
								  ON
								  DL_MODELES_CHAMPS.ID_DL = DL_VUES_VIRTUELLES.ID_DL
								  AND
								  DL_MODELES_CHAMPS.ID_VUE = DL_VUES_VIRTUELLES.ID_VUE_VIRTUELLE
								  WHERE 
								  DL_MODELES_CHAMPS.ID_MC = '".${'chaine_champ_filtre_champ'.$p}."'
								  AND
								  DL_MODELES_CHAMPS.ID_DL = '".$nom_connecteur."'
								  AND
								  DL_VUES_VIRTUELLES.ID = '".${'chaine_champ_filtre_virtuel_dossier'.$p}."'
									");
						${'fetch_champ_filtre_virtuelle'.$p} = ${'requete_champ_filtre_virtuelle'.$p}->fetch();
						${'nom_filtre_champ_remontee'.$p} = ${'fetch_champ_filtre_virtuelle'.$p}['desig'];
						${'nom_vue_virtuelle'.$p} = ${'fetch_champ_filtre_virtuelle'.$p}['desig_vue'];
						${'recup_valeur_filtre'.$p} = strstr(${'chaine_filtre'.$p},'V=');
						${'valeur_filtre'.$p} = substr(${'recup_valeur_filtre'.$p},2);
						
						if(strlen(${'chaine_champ_filtre_champ'.$p}) === 0 XOR strlen(${'nom_filtre_champ_remontee'.$p}) === 0)
						{
							${'nom_filtre_champ'.$p} = 'Filtre '.$p.' inconnu (virtualisé - pas sur la base -<em> ID remonté : '.${'chaine_champ_filtre'.$p}.'</em>)';
						}

						else
						{	
							${'nom_filtre_champ'.$p} = ${'nom_filtre_champ_remontee'.$p}. '<em></strong> (Champ virtualisé - Modèle d\'origine : '.${'nom_vue_virtuelle'.$p}.')</em>';
						}
					}
					else // Si ce n'est pas un dossier virtuel
					{
							${'nom_filtre_champ'.$p} = 'Filtre '.$p.' inconnu (ID retrouvé : '.${'chaine_champ_filtre_champ'.$p}.') - Valeur : "'.${'valeur_filtre'.$p}.'"<br>';
					}		
				}		
				
				// Mise en place pour retrouver les références aux cellules que font les filtres (si référence il y a)
				${'accolade_filtre_debut'.$p} = strpos(${'valeur_filtre'.$p},'{');
				${'accolade_filtre_fin'.$p} = strpos(${'valeur_filtre'.$p},'}');
				
				if(is_numeric(${'accolade_filtre_debut'.$p}) AND is_numeric(${'accolade_filtre_fin'.$p})) // Si on a bien une ref. à un cellule du classeur
					{
						$s = (str_replace('}','',(str_replace('{','',${'valeur_filtre'.$p})))+1); // Trouver le numéro ref de la cellule
						${'valeur_filtre'.$p} = ${'cellule_ref_filtres'.$s}; // On fait référence à la ref défini plus haut
					}	
				
				// Définition finale du filtre
						
				if($p === 1) // Si on est au premier filtre, le traitement est différent  (on ajout l'en-tête du paragraphe filtre)
				{
					${'nom_du_filtre'.$p} = '<br><span class="titre_params_rubriques">Détails filtre(s) :</span> <hr>
					Filtre sur : <strong>'.${'nom_filtre_champ'.$p}.'</strong> - Valeur : "'.${'valeur_filtre'.$p}.'"<br/><br/>';	
				}
				else
				{
					${'nom_du_filtre'.$p} = 'Filtre sur : <strong>'.${'nom_filtre_champ'.$p}.'</strong> - Valeur : "'.${'valeur_filtre'.$p}.'"<br/><br/>';
				}	
				$p++;

			}
				
		if ($presence_filtres_avancees <> NULL) // Si on a des filtres avancés ou non - A CORRIGER SI REF. CELLULE
		{
			$chaine_filtres_avances_base = strstr(str_replace('"','',$valeur_axe_filtres), ':@');
			$chaine_filtres_avances = substr($chaine_filtres_avances_base,2);
			$filtres_avances = 'Filtres avancés : "'.$chaine_filtres_avances. '"<br/<br/>';
		}	
				
		else
		{
			$filtres_avances = '';
		}
				
	}
			
	else
	{
			$nb_de_filtres = 1;
			$nom_du_filtre1 = '';
			$filtres_avances = '';
	}	

$nom_axe_montant = '';
$nom_du_mod_champs =  '';
$nom_source_mod_champs = '';
$nom_axe_agreg = '';
$nom_formulax = '';
$nom_axe_filtres = '';
$nb_de_champs_ali = 0;
$nb_de_caches = '';
$detail_cache1 = '';
$nb_de_caches_numeric = 0;

?>