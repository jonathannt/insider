<!DOCTYPE html>

<html>

    <head>

        <meta charset="utf-8" />
        <title>Insider - Connexion à la MasterReporting</title>
		<meta name="description" content="Insider" />
		<link rel="stylesheet" href="../inside.css" />
        <!--[if IE]><link rel="shortcut icon" type="image/x-icon" href="favicon.ico" /><![endif]-->		
		<script type="text/javascript">
		function variables() { // Définition des variable au chargement de la fenêtre
		text_principal = document.getElementById('resultat_connecteurs');
		illimites = document.getElementById('illimites');
		x00001 = document.getElementById('x00001');
		x00002 = document.getElementById('x00002');
		x00003 = document.getElementById('x00003');
		x1000Compta = document.getElementById('x1000Compta');
		x1000Immo = document.getElementById('x1000Immo');
		x1000Tréso = document.getElementById('x1000Tréso');
		x1000PO = document.getElementById('x1000PO');
		x1000ComptaOracle = document.getElementById('x1000ComptaOracle');
		x1000ImmoOracle = document.getElementById('x1000ImmoOracle');
		x1000POOracle = document.getElementById('x1000POOracle');
		x1000TresoOracle = document.getElementById('x1000TresoOracle');
		x100cCompta = document.getElementById('x100cCompta');
		x100cPaie = document.getElementById('x100cPaie');
		x100cGesCo = document.getElementById('x100cGesCo');
		x100cTreso = document.getElementById('x100cTreso');
		x100cImmo = document.getElementById('x100cImmo');
		x100cProd = document.getElementById('x100cProd');
		xrtTreasury = document.getElementById('xrtTreasury');
		totem = document.getElementById('totem');
		x3Compta = document.getElementById('x3Compta');
		atheneo = document.getElementById('atheneo');
		SMDE = document.getElementById('SMDE');
		CegidCompta = document.getElementById('CegidCompta');
		Divalto = document.getElementById('Divalto');
		EBP = document.getElementById('EBP');
		}
		function coche_perso() { // En fonction de ce que l'on coche
		
		if (illimites.checked) {illimites.value = ',%';} else {illimites.value = '';}
		if (x00001.checked) {x00001.value = ',0001';} else {x00001.value = '';}
		if (x00002.checked) {x00002.value = ',0002';} else {x00002.value = '';}
		if (x00003.checked) {x00003.value = ',0003';} else {x00003.value = '';}
		if (x1000Compta.checked) {x1000Compta.value = ',INF06';} else {x1000Compta.value = '';}
		if (x1000Immo.checked) {x1000Immo.value = ',INF18';} else {x1000Immo.value = '';}
		if (x1000Tréso.checked) {x1000Tréso.value = ',INF08';} else {x1000Tréso.value = '';}
		if (x1000PO.checked) {x1000PO.value = ',INF34';} else {x1000PO.value = '';}
		if (x1000ComptaOracle.checked) {x1000ComptaOracle.value = ',INF16';} else {x1000ComptaOracle.value = '';}
		if (x1000ImmoOracle.checked) {x1000ImmoOracle.value = ',INF50';} else {x1000ImmoOracle.value = '';}
		if (x1000POOracle.checked) {x1000POOracle.value = ',INF51';} else {x1000POOracle.value = '';}
		if (x1000TresoOracle.checked) {x1000TresoOracle.value = ',INF52';} else {x1000TresoOracle.value = '';}
		if (x100cCompta.checked) {x100cCompta.value = ',INF02';} else {x100cCompta.value = '';}
		if (x100cPaie.checked) {x100cPaie.value = ',INF04';} else {x100cPaie.value = '';}
		if (x100cGesCo.checked) {x100cGesCo.value = ',INF12';} else {x100cGesCo.value = '';}
		if (x100cTreso.checked) {x100cTreso.value = ',INF35';} else {x100cTreso.value = '';}
		if (x100cImmo.checked) {x100cImmo.value = ',INF36';} else {x100cImmo.value = '';}
		if (x100cProd.checked) {x100cProd.value = ',INF47';} else {x100cProd.value = '';}
		if (xrtTreasury.checked) {xrtTreasury.value = ',INF01';} else {xrtTreasury.value = '';}
		if (totem.checked) {totem.value = ',INF05';} else {totem.value = '';}
		if (x3Compta.checked) {x3Compta.value = ',INF17';} else {x3Compta.value = '';}
		if (atheneo.checked) {atheneo.value = ',INF20';} else {atheneo.value = '';}
		if (SMDE.checked) {SMDE.value = ',INF19';} else {SMDE.value = '';}
		if (CegidCompta.checked) {CegidCompta.value = ',INF22';} else {CegidCompta.value = '';}
		if (Divalto.checked) {Divalto.value = ',INF44';} else {Divalto.value = '';}
		if (EBP.checked) {EBP.value = ',INF48';} else {EBP.value = '';}
		
		
		// Concaténation des valeurs
chaine_licence = illimites.value+x00001.value+x00002.value+x00003.value+xrtTreasury.value+x100cCompta.value+x100cPaie.value+totem.value+x1000Compta.value+x1000Tréso.value+x100cGesCo.value+x1000ComptaOracle.value+x3Compta.value+x1000Immo.value+SMDE.value+atheneo.value+CegidCompta.value+x1000PO.value+x100cTreso.value+x100cImmo.value+Divalto.value+x100cProd.value+EBP.value+x1000ImmoOracle.value+x1000POOracle.value+x1000TresoOracle.value;
		text_principal.value = chaine_licence.substring(1,50000); // Affichage des valeurs cochées (en enlevant la virgule du début)
		}
		</script>
	</head>
	<body class="back_parametres" onload="variables()">

<h3 class="titres_params"><strong>CHOISISSEZ VOS CONNECTEURS :</strong></h3>
<br><br>
<div>
<strong>SPÉCIFIQUES :</strong>
<br><br>
<input type="checkbox" id="illimites" name="connecteur" value="%" onclick="coche_perso()"><label for="illimites">Illimités "%"</label><br>
<input type="checkbox" id="x00001" name="connecteur" value="0001" onclick="coche_perso()"><label for="x00001">00001 "0001" (Personnalisé)</label><br>
<input type="checkbox" id="x00002" name="connecteur" value="0002" onclick="coche_perso()"><label for="x00002">00002 "0002" (Personnalisé)</label><br>
<input type="checkbox" id="x00003" name="connecteur" value="0003" onclick="coche_perso()"><label for="x00003">00003 "0003" (Personnalisé)</label><br>
<br><br>
</div>

<strong>CONNECTEURS "STANDARDS" :</strong>
<br><br>
<div class="left">
<strong>Connecteur Sage FRP 1000 :</strong>
<br>
<input type="checkbox" id="x1000Compta" name="connecteur" value="INF06" onclick="coche_perso()"><label for="x1000Compta">Sage FRP 1000 Comptabilité "INF06"</label><br>
<input type="checkbox" id="x1000Tréso" name="connecteur" value="INF08" onclick="coche_perso()"><label for="x1000Tréso">Sage FRP 1000 Trésorerie "INF08"</label><br>
<input type="checkbox" id="x1000Immo" name="connecteur" value="INF18" onclick="coche_perso()"><label for="x1000Immo">Sage FRP 1000 Immobilisations "INF18"</label><br>
<input type="checkbox" id="x1000PO" name="connecteur" value="INF34" onclick="coche_perso()"><label for="x1000PO">Sage FRP 1000 Processus Opérationnels "INF34"</label><br>
<input type="checkbox" id="x1000ComptaOracle" name="connecteur" value="INF16" onclick="coche_perso()"><label for="x1000ComptaOracle">Sage FRP 1000 Comptabilité <strong>ORACLE</strong> "INF16"</label><br>
<input type="checkbox" id="x1000TresoOracle" name="connecteur" value="INF52" onclick="coche_perso()"><label for="x1000TresoOracle">Sage FRP 1000 Trésorerie <strong>ORACLE</strong> "INF52"</label><br>
<input type="checkbox" id="x1000ImmoOracle" name="connecteur" value="INF50" onclick="coche_perso()"><label for="x1000ImmoOracle">Sage FRP 1000 Immobilisation <strong>ORACLE</strong> "INF50"</label><br>
<input type="checkbox" id="x1000POOracle" name="connecteur" value="INF51" onclick="coche_perso()"><label for="x1000POOracle">Sage FRP 1000 Proc. Opérationnels <strong>ORACLE</strong> "INF51"</label><br>
</div>
<div class="middle">
<strong>Connecteur Sage 100c :</strong>
<br>
<input type="checkbox" id="x100cCompta" name="connecteur" value="INF02" onclick="coche_perso()"><label for="x100cCompta">Sage 100c Comptabilité "INF02"</label><br>
<input type="checkbox" id="x100cPaie" name="connecteur" value="INF04" onclick="coche_perso()"><label for="x100cPaie">Sage 100c Paie "INF04"</label><br>
<input type="checkbox" id="x100cGesCo" name="connecteur" value="INF12" onclick="coche_perso()"><label for="x100cGesCo">Sage 100c Gestion Commerciale "INF12"</label><br>
<input type="checkbox" id="x100cTreso" name="connecteur" value="INF35" onclick="coche_perso()"><label for="x100cTreso">Sage 100c Trésorerie "INF35"</label><br>
<input type="checkbox" id="x100cImmo" name="connecteur" value="INF36" onclick="coche_perso()"><label for="x100cImmo">Sage 100c Immobilisations "INF36"</label><br>
<input type="checkbox" id="x100cProd" name="connecteur" value="INF47" onclick="coche_perso()"><label for="x100cProd">Sage 100c Gestion de Production "INF47"</label><br>
</div>
<div class="right">
<strong>Autres Connecteurs "Standards":</strong>
<br>
<input type="checkbox" id="totem" name="connecteur" value="INF05" onclick="coche_perso()"><label for="totem">Totem Comptabilité "INF05"</label><br>
<input type="checkbox" id="x3Compta" name="connecteur" value="INF17" onclick="coche_perso()"><label for="x3Compta">Sage X3 Comptabilité "INF17"</label><br>
<input type="checkbox" id="atheneo" name="connecteur" value="INF20" onclick="coche_perso()"><label for="atheneo">Athénéo "INF20"</label><br>
<br><br><br>
</div>
<br><br>
<div >
<strong>CONNECTEURS "STARTERS" :</strong>
<br>
</div>
<div class="left">
<strong>Sage :</strong>
<br>
<input type="checkbox" id="xrtTreasury" name="connecteur" value="INF01" onclick="coche_perso()"><label for="xrtTreasury">Sage XRT Treasury "INF01"</label><br>
<input type="checkbox" id="SMDE" name="connecteur" value="INF19" onclick="coche_perso()"><label for="SMDE">Sage Multi-Devis "INF19"</label><br>
<br><br>
</div>
<div class="middle">
<br>
<strong>Autres:</strong>
<br>
<input type="checkbox" id="CegidCompta" name="connecteur" value="INF22" onclick="coche_perso()"><label for="CegidCompta">Cegid Comptabilité "INF22"</label><br>
<input type="checkbox" id="Divalto" name="connecteur" value="INF44" onclick="coche_perso()"><label for="Divalto">Divalto "INF44"</label><br>
<input type="checkbox" id="EBP" name="connecteur" value="INF48" onclick="coche_perso()"><label for="EBP">EBP (Compta et GesCo) "INF48"</label><br>
<br>
</div>
<br><br>
<textarea id="resultat_connecteurs" spellcheck="false" rows="5" cols="150">

<?php				
// echo '%,';
// for($k=1; $k<100; $k++)
// {
	// echo 'INF';
	// if($k<10)
		// {echo '0';}
	// else
		// {echo'';}
	// echo $k;
		// if ($k<99)	
		// {echo ',';}
		// else
		// {echo '';}	
// }
?>
</textarea>

	</body>
</html>